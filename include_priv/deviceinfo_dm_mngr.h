/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DEVICEINFO_DM_MNGR_H__)
#define __DEVICEINFO_DM_MNGR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "dm_deviceinfo.h"

// datamodel manager common functions
int PRIVATE dm_mngr_create(amxd_object_t* object,
                           amxc_var_t* parameters,
                           amxc_var_t* ret);

int PRIVATE dm_mngr_update(amxd_object_t* object,
                           amxc_var_t* parameters);

int PRIVATE dm_mngr_remove(amxd_object_t* object,
                           uint32_t id);

const char* PRIVATE deviceinfo_get_so_name(void);

void PRIVATE deviceinfo_dm_mngr_clean(void);

amxd_status_t _UpTime_uptime_onread(amxd_object_t* const object,
                                    amxd_param_t* const param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv);


amxd_status_t _Total_total_onread(amxd_object_t* const object,
                                  amxd_param_t* const param,
                                  amxd_action_t reason,
                                  const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  void* priv);

amxd_status_t _Free_free_onread(amxd_object_t* const object,
                                amxd_param_t* const param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv);

amxd_status_t _call_ctrl(amxd_object_t* deviceinfo,
                         amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret);

void _hostname_changed(const char* const sig_name,
                       const amxc_var_t* const event_data,
                       void* const priv);

void _last_upgrade_changed(const char* const sig_name,
                           const amxc_var_t* const event_data,
                           void* const priv);

void _syslog_start_sync(const char* const sig_name,
                        const amxc_var_t* const event_data,
                        void* const priv);

void _controllers_reload(const char* const sig_name,
                         const amxc_var_t* const event_data,
                         void* const priv);
// data model manager interface - used by controllers

#ifdef __cplusplus
}
#endif

#endif // __DEVICEINFO_DM_MNGR_H__
