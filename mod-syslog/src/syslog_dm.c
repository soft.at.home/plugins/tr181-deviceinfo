/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "syslog_dm.h"
#include "syslog_events.h"
#include "syslog_helpers.h"
#include "mod_syslog.h"

static bool waiting_for_syslog = false;

static void wait_done_cb(UNUSED const char* const signal,
                         UNUSED const amxc_var_t* const date,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "wait:done received");

    amxp_slot_disconnect(NULL, "wait:done", wait_done_cb);
    waiting_for_syslog = false;

    populate_deviceinfo_vendorlogfiles();
    init_syslog_subscriptions();
    SAH_TRACEZ_OUT(ME);
}

void wait_for_syslog(void) {
    SAH_TRACEZ_IN(ME);

    when_false(syslog_sync_needed(), exit);
    when_true(waiting_for_syslog, exit);
    if(amxb_be_who_has(SYSLOG_PATH) != NULL) {
        wait_done_cb(NULL, NULL, NULL);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "wait for " SYSLOG_PATH " object");
    amxp_slot_connect(NULL, "wait:done", NULL, wait_done_cb, NULL);
    amxb_wait_for_object(SYSLOG_PATH); // works async
    waiting_for_syslog = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void populate_deviceinfo_vendorlogfiles(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxb_bus_ctx_t* ctx = syslog_ctx();
    amxc_var_t data;
    amxc_var_t updated_files;

    amxc_var_init(&data);
    amxc_var_init(&updated_files);

    when_false(syslog_sync_needed(), exit);

    // Get existing Syslog.Action.*.Logfile instances
    when_null_trace(ctx, exit, ERROR, "Could not find Syslog bus context");
    rv = amxb_get(ctx, SYSLOG_PATH "Action.", INT32_MAX, &data, 5);
    when_failed_trace(rv, exit, ERROR, "amxb_get failed rv=%d", rv);

    amxc_var_set_type(&updated_files, AMXC_VAR_ID_LIST);

    // Loop over syslog action logfiles and create a list of htables containing the settings for the new deviceinfo vendorlogfiles
    amxc_llist_for_each(data_it, amxc_var_constcast(amxc_llist_t, &data)) {
        amxc_var_t* logfile_htable = amxc_container_of(data_it, amxc_var_t, lit);
        amxc_htable_for_each(htable_it, amxc_var_constcast(amxc_htable_t, logfile_htable)) {
            const char* syslog_action_logfile_path = amxc_htable_it_get_key(htable_it);
            amxc_var_t* syslog_action_logfile_parameters = amxc_var_from_htable_it(htable_it);
            amxc_var_t* list_item = NULL;
            const char* syslog_action_alias = NULL;
            amxc_string_t syslog_action_path;
            amxc_string_t deviceinfo_vendorlogfile_alias;

            amxc_string_init(&syslog_action_path, 0);
            amxc_string_init(&deviceinfo_vendorlogfile_alias, 0);

            // For each added Syslog.Action.{i}.LogFile object we also have a Syslog.Action. and a Syslog.Action.{i}. object data in the logfile_htable.
            // We are only interested in FilePath data which is exlusively in Syslog.Action.{i}.LogFile
            if(STR_EMPTY(GET_CHAR(syslog_action_logfile_parameters, "FilePath"))) {
                amxc_string_clean(&deviceinfo_vendorlogfile_alias);
                amxc_string_clean(&syslog_action_path);
                continue;
            }

            // In order to get the alias out of the correct Syslog Action object data (see comment above), remove the LogFile child object from the path.
            // The content of the string should be something like Syslog.Action.{i}
            amxc_string_setf(&syslog_action_path, "%s", syslog_action_logfile_path);
            amxc_string_replace(&syslog_action_path, "LogFile.", "", 1);

            // Get the alias of Syslog.Action.{i}
            syslog_action_alias = GET_CHAR(GET_ARG(logfile_htable, amxc_string_get(&syslog_action_path, 0)), "Alias");
            amxc_string_setf(&deviceinfo_vendorlogfile_alias, VENDORLOGFILE_SYSLOG_ALIAS_PREFIX "%s", syslog_action_alias);

            list_item = amxc_var_add_new(&updated_files);
            rv = populate_logfile_settings_htable("add", list_item, syslog_action_logfile_parameters, syslog_action_logfile_path, amxc_string_get(&deviceinfo_vendorlogfile_alias, 0));

            amxc_string_clean(&deviceinfo_vendorlogfile_alias);
            amxc_string_clean(&syslog_action_path);
            when_failed(rv, exit);
        }
    }

    // Update DeviceInfo and Syslog Datamodels
    update_datamodels(&updated_files, true);

exit:
    amxc_var_clean(&updated_files);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

static int update_syslog_action_logfiles(amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    when_null_trace(syslog_ctx(), exit, ERROR, "Could not find Syslog bus context");

    amxc_llist_for_each(data_it, amxc_var_constcast(amxc_llist_t, data)) {
        amxc_var_t* logfile_htable = amxc_container_of(data_it, amxc_var_t, lit);
        const char* syslog_action_logfile_path = GET_CHAR(logfile_htable, "SyslogLogFileRef");
        const char* deviceinfo_vendorlogfile_path = GET_CHAR(logfile_htable, "SyslogVendorLogFileRef");

        when_str_empty(syslog_action_logfile_path, exit);
        when_str_empty(deviceinfo_vendorlogfile_path, exit);

        // Set the DeviceInfo.VendorLogFile path on the correct Syslog.Action.Logfile
        if(strcmp(GET_CHAR(logfile_htable, "Action"), "add") == 0) {
            amxc_var_t parameters;
            amxc_var_t ret;

            amxc_var_init(&parameters);
            amxc_var_init(&ret);

            amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, &parameters, "VendorLogFileRef", deviceinfo_vendorlogfile_path);

            SAH_TRACEZ_INFO(ME, "Set %sVendorLogFileRef to '%s'", syslog_action_logfile_path, deviceinfo_vendorlogfile_path);
            rv = amxb_set(syslog_ctx(), syslog_action_logfile_path, &parameters, &ret, 5);
            amxc_var_clean(&ret);
            amxc_var_clean(&parameters);

            when_failed_trace(rv, exit, ERROR, "Could not set %sVendorLogFileRef to '%s'", syslog_action_logfile_path, deviceinfo_vendorlogfile_path);
        }
    }
    rv = 0;

exit:
    return rv;
    SAH_TRACEZ_OUT(ME);
}

int update_datamodels(amxc_var_t* updated_files, bool update_syslog_dm) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&ret);
    when_false_status(syslog_sync_needed(), exit, rv = 0);

    if(sync_vendorlogfiles_allowed()) {
        // Update the DeviceInfo.VendorLogFile instances
        rv = amxm_execute_function("self", MOD_CORE, "update-vendorlogfile", updated_files, &ret);
        when_failed_trace(rv, exit, WARNING, "Mod %s, func 'update-vendorlogfile' returned %d", MOD_CORE, rv);

        // If needed also update the Syslog.Action.LogFile instances
        if(update_syslog_dm) {
            rv = update_syslog_action_logfiles(updated_files);
            when_failed_trace(rv, exit, WARNING, "Could not update VendorLogFileRef for Syslog objects");
        }
    }

    if(sync_logrotate_allowed()) {
        rv = amxm_execute_function("self", MOD_CORE, "update-logrotate", updated_files, &ret);
        when_failed_trace(rv, exit, WARNING, "Mod %s, func 'update-logrotate' returned %d", MOD_CORE, rv);
    }

exit:
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
