/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <time.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>
#include <amxut/amxut_bus.h>

#include "mock.h"

#define ME "test"

static int count = 0;
static amxc_var_t assigned_vendor_refs;
static amxc_var_t assigned_logrotates;

void assert_nr_deviceinfo_vendorlogfiles(int32_t expected) {
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &assigned_vendor_refs)), expected);
}

void assert_nr_deviceinfo_logrotates(int32_t expected) {
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &assigned_logrotates)), expected);
}

void assert_deviceinfo_vendorlogfile(const char* path, const char* file) {
    if(allow_vendorlogfiles) {
        amxc_var_t* path_key = amxc_var_get_key(&assigned_vendor_refs, file, AMXC_VAR_FLAG_DEFAULT);
        assert_non_null(path_key);

        assert_string_equal(path, GET_CHAR(path_key, NULL));
    }
}

void assert_deviceinfo_logrotate(const char* path, const char* alias) {
    if(allow_logrotate) {
        amxc_var_t* path_key = amxc_var_get_key(&assigned_logrotates, alias, AMXC_VAR_FLAG_DEFAULT);
        assert_non_null(path_key);

        assert_string_equal(path, GET_CHAR(path_key, NULL));
    }
}

void init_test_variants(void) {
    if(amxc_var_is_null(&assigned_vendor_refs)) {
        amxc_var_init(&assigned_vendor_refs);
        amxc_var_set_type(&assigned_vendor_refs, AMXC_VAR_ID_HTABLE);
    }
    if(amxc_var_is_null(&assigned_logrotates)) {
        amxc_var_init(&assigned_logrotates);
        amxc_var_set_type(&assigned_logrotates, AMXC_VAR_ID_HTABLE);
    }
}

void cleanup_test_variants(void) {
    amxc_var_clean(&assigned_vendor_refs);
    amxc_var_clean(&assigned_logrotates);
    count = 0;
}

static int wrap_update_vendorlogfile(amxc_var_t* args) {
    assert_non_null(args);

    // Loop over list of all parameters used to update these VendorLogFile instances
    amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, args)) {
        amxc_var_t* parameters = amxc_var_from_llist_it(it);
        const char* action = GET_CHAR(parameters, "Action");

        assert_non_null(GET_CHAR(parameters, "SyslogActionLogFilePath"));

        if(strcmp(action, "add") == 0) {
            amxc_string_t buffer;
            amxc_var_t* syslog_logfile_vendor_logfile_ref_key = amxc_var_get_key(parameters, "SyslogVendorLogFileRef", AMXC_VAR_FLAG_DEFAULT);

            amxc_string_init(&buffer, 0);

            assert_non_null(syslog_logfile_vendor_logfile_ref_key);
            assert_non_null(GET_CHAR(parameters, "SyslogActionAlias"));
            assert_non_null(GET_CHAR(parameters, "SyslogVendorLogFileRef"));

            if(amxc_var_get_key(&assigned_vendor_refs, GET_CHAR(parameters, "SyslogActionLogFilePath"), AMXC_VAR_FLAG_DEFAULT) != NULL) {
                amxc_string_setf(&buffer, "%s", GET_CHAR(&assigned_vendor_refs, GET_CHAR(parameters, "SyslogActionLogFilePath")));
            } else {
                amxc_string_setf(&buffer, "DeviceInfo.VendorLogFile.%d.", ++count);
                amxc_var_add_key(cstring_t, &assigned_vendor_refs, GET_CHAR(parameters, "SyslogActionLogFilePath"), amxc_string_get(&buffer, 0));
            }

            amxc_var_set(cstring_t, syslog_logfile_vendor_logfile_ref_key, amxc_string_get(&buffer, 0));
            amxc_string_clean(&buffer);
            break;
        } else if(strcmp(action, "update") == 0) {
            assert_non_null(GET_CHAR(parameters, "SyslogVendorLogFileRef"));
            amxc_htable_for_each(htable_it, amxc_var_constcast(amxc_htable_t, &assigned_vendor_refs)) {
                amxc_var_t* vendor_ref = amxc_var_from_htable_it(htable_it);

                if(strcmp(GET_CHAR(vendor_ref, NULL), GET_CHAR(parameters, "SyslogVendorLogFileRef")) == 0) {
                    amxc_htable_it_take(htable_it);
                    amxc_var_delete(&vendor_ref);
                    break;
                }
            }
            amxc_var_add_key(cstring_t, &assigned_vendor_refs, GET_CHAR(parameters, "SyslogActionLogFilePath"), GET_CHAR(parameters, "SyslogVendorLogFileRef"));
        } else if(strcmp(action, "remove") == 0) {
            amxc_var_t* path_key = amxc_var_get_key(&assigned_vendor_refs, GET_CHAR(parameters, "SyslogActionLogFilePath"), AMXC_VAR_FLAG_DEFAULT);
            assert_non_null(path_key);
            amxc_var_delete(&path_key);
        } else {
            assert_true(false);
        }
    }
    return 0;
}

static int wrap_update_logrotate(amxc_var_t* args) {
    assert_non_null(args);

    amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, args)) {
        amxc_var_t* parameters = amxc_var_from_llist_it(it);
        const char* logrotate_alias = GET_CHAR(parameters, "SyslogActionAlias");
        const char* action = GET_CHAR(parameters, "Action");
        assert_non_null(action);
        assert_non_null(logrotate_alias);

        if(strcmp(action, "add") == 0) {
            if(amxc_var_get_key(&assigned_logrotates, GET_CHAR(parameters, "SyslogActionLogFilePath"), AMXC_VAR_FLAG_DEFAULT) == NULL) {
                amxc_var_add_key(cstring_t, &assigned_logrotates, GET_CHAR(parameters, "SyslogActionLogFilePath"), logrotate_alias);
            }

            break;
        } else if(strcmp(action, "update") == 0) {
            amxc_htable_for_each(htable_it, amxc_var_constcast(amxc_htable_t, &assigned_logrotates)) {
                amxc_var_t* vendor_ref = amxc_var_from_htable_it(htable_it);

                if(strcmp(GET_CHAR(vendor_ref, NULL), logrotate_alias) == 0) {
                    amxc_htable_it_take(htable_it);
                    amxc_var_delete(&vendor_ref);
                    break;
                }
            }
            amxc_var_add_key(cstring_t, &assigned_logrotates, GET_CHAR(parameters, "SyslogActionLogFilePath"), logrotate_alias);
        } else if(strcmp(action, "remove") == 0) {
            amxc_var_t* path_key = amxc_var_get_key(&assigned_logrotates, GET_CHAR(parameters, "SyslogActionLogFilePath"), AMXC_VAR_FLAG_DEFAULT);
            assert_non_null(path_key);
            amxc_var_delete(&path_key);
        } else {
            assert_true(false);
        }
    }

    return 0;
}

static int wrap_get_syslog_config(amxc_var_t* args) {
    assert_non_null(args);

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, args, "sync_vendorlog", allow_vendorlogfiles);
    amxc_var_add_key(bool, args, "sync_logrotate", allow_logrotate);

    return 0;
}

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    int rv = -1;
    assert_string_equal(shared_object_name, "self");
    assert_string_equal(module_name, "dm-mngr");
    assert_non_null(args);
    assert_non_null(ret);

    if(strcmp(func_name, "update-vendorlogfile") == 0) {
        rv = wrap_update_vendorlogfile(args);
    } else if(strcmp(func_name, "update-logrotate") == 0) {
        rv = wrap_update_logrotate(args);
    } else if(strcmp(func_name, "get-syslog-config") == 0) {
        rv = wrap_get_syslog_config(args);
    }
    return rv;
}
