include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C system/src all
	$(MAKE) -C mod-lr-logrotate/src all
	$(MAKE) -C mod-syslog/src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C system/src clean
	$(MAKE) -C mod-lr-logrotate/src clean
	$(MAKE) -C mod-syslog/src clean
	$(MAKE) -C odl clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/deviceinfo-system.so $(DEST)/usr/lib/amx/deviceinfo-system/deviceinfo-system.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_controllers.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_controllers.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(foreach odl,$(wildcard odl/$(COMPONENT)_defaults/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/deviceinfo-system/defaults.d
	$(foreach odl,$(wildcard system/odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/deviceinfo-system/defaults.d/;)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-deviceinfo_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_mac_address_rpc.odl $(DEST)/etc/amx/$(COMPONENT)/deviceinfo_mac_address_rpc.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo-manager_logrotate_scripts.odl $(DEST)/etc/amx/$(COMPONENT)/deviceinfo-manager_logrotate_scripts.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_prefix.odl $(DEST)/etc/amx/$(COMPONENT)/deviceinfo_prefix.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_cert_path.odl $(DEST)/etc/amx/$(COMPONENT)/deviceinfo_cert_path.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_vendorlogfile_1.odl $(DEST)/etc/amx/$(COMPONENT)/deviceinfo_vendorlogfile_1.odl
	$(INSTALL) -D -p -m 0644 system/odl/deviceinfo-system.odl $(DEST)/etc/amx/deviceinfo-system/deviceinfo-system.odl
	$(INSTALL) -D -p -m 0644 system/odl/deviceinfo-system_definition.odl $(DEST)/etc/amx/deviceinfo-system/deviceinfo-system_definition.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/untrusted/$(COMPONENT).json $(DEST)$(ACLDIR)/untrusted/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/deviceinfo-system
ifeq ($(CONFIG_SAH_AMX_TR181_DEVICEINFO_DEVELOPMENT),y)
	$(INSTALL) -D -p -m 0755 scripts/00_core_dump_limit.sh $(DEST)/etc/env.d/00_core_dump_limit.sh
endif
	$(INSTALL) -D -p -m 0755 scripts/00_environment_deviceinfo.sh $(DEST)/etc/env.d/00_environment_deviceinfo.sh
ifeq ($(CONFIG_ACCESSPOINT),y)
ifneq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_ap_hostname.sh $(DEST)/etc/env.d/00_ap_hostname.sh
endif
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_gw_hostname.sh $(DEST)/etc/env.d/00_gw_hostname.sh
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
ifneq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_ap_friendlyname.sh $(DEST)/etc/env.d/00_ap_friendlyname.sh
endif
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_gw_friendlyname.sh $(DEST)/etc/env.d/00_gw_friendlyname.sh
endif
	$(INSTALL) -D -p -m 0755 scripts/00_environment_mfg_helper.sh $(DEST)/etc/env.d/00_environment_mfg_helper.sh
	$(INSTALL) -D -p -m 0755 scripts/01_calculate_mac_addresses_from_basemacaddress.sh $(DEST)/etc/env.d/01_calculate_mac_addresses_from_basemacaddress.sh
	$(INSTALL) -D -p -m 0755 scripts/00_environment_xclone_macaddres.sh $(DEST)/etc/env.d/00_environment_xclone_macaddres.sh
	$(INSTALL) -D -p -m 0755 scripts/01_calculate_mac_addresses_from_basemacaddress.sh $(DEST)/etc/env.d/01_calculate_mac_addresses_from_basemacaddress.sh
	$(INSTALL) -D -p -m 0755 scripts/environment.sh $(DEST)$(INITDIR)/deviceinfo-environment
	$(INSTALL) -d -m 0755 $(DEST)/etc
	ln -sfr $(DEST)/var/etc/environment $(DEST)/etc/environment
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/deviceinfo-system.sh $(DEST)$(INITDIR)/deviceinfo-system
ifeq ($(CONFIG_SAH_AMX_ADD_SYSINIT_DONE_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/sysinit_done.sh $(DEST)$(INITDIR)/z_sysinit_done
endif
ifeq ($(CONFIG_SAH_AMX_ADD_DBREADY_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/dbready.sh $(DEST)$(INITDIR)/w_dbready
endif
ifeq ($(CONFIG_SAH_AMX_ADD_DBREADY_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/software_version_hash.sh $(DEST)/usr/sbin/software_version_hash.sh
endif
	$(INSTALL) -D -p -m 0755 scripts/debug_lcm_containers.sh $(DEST)/usr/lib/debuginfo/debug_lcm_containers.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_memory.sh $(DEST)/usr/lib/debuginfo/debug_memory.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_process.sh $(DEST)/usr/lib/debuginfo/debug_process.sh
ifeq ($(CONFIG_SAH_AMX_ADD_DBREADY_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/trigger_restore.sh $(DEST)/lib/preinit/98_trigger_restore
endif
	$(INSTALL) -D -p -m 0755 scripts/logrotate_add.sh $(DEST)/usr/lib/debuginfo/logrotate_add.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_remove.sh $(DEST)/usr/lib/debuginfo/logrotate_remove.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_mv.sh $(DEST)/usr/lib/debuginfo/logrotate_mv.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_append.sh $(DEST)/usr/lib/debuginfo/logrotate_append.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_var.sh $(DEST)/usr/lib/debuginfo/logrotate_var.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_logrotate.sh $(DEST)/usr/lib/debuginfo/debug_logrotate.sh
	$(INSTALL) -D -p -m 0755 scripts/tmpfs_download_var.sh $(DEST)/usr/lib/amx/scripts/tmpfs_download_var.sh
	$(INSTALL) -D -p -m 0755 scripts/tmpfs_download_mount.sh $(DEST)/usr/lib/amx/scripts/tmpfs_download_mount.sh
	$(INSTALL) -D -p -m 0755 scripts/tmpfs_download_unmount.sh $(DEST)/usr/lib/amx/scripts/tmpfs_download_unmount.sh
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-lr-logrotate.so $(DEST)/usr/lib/amx/modules/mod-lr-logrotate.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-syslog.so $(DEST)/usr/lib/amx/modules/mod-syslog.so
ifeq ($(CONFIG_SAH_AMX_TR181_DEVICEINFO_REQUIRES_USERS),y)
	$(INSTALL) -D -p -m 0644 odl/extensions/$(COMPONENT)_requires_users.odl $(DEST)/etc/amx/$(COMPONENT)/extensions/$(COMPONENT)_requires_users.odl
endif

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/deviceinfo-system.so $(PKGDIR)/usr/lib/amx/deviceinfo-system/deviceinfo-system.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_controllers.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_controllers.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/deviceinfo-system/defaults.d
	$(INSTALL) -D -p -m 0644 system/odl/defaults.d/*.odl $(PKGDIR)/etc/amx/deviceinfo-system/defaults.d/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-deviceinfo_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_mac_address_rpc.odl $(PKGDIR)/etc/amx/$(COMPONENT)/deviceinfo_mac_address_rpc.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo-manager_logrotate_scripts.odl $(PKGDIR)/etc/amx/$(COMPONENT)/deviceinfo-manager_logrotate_scripts.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_prefix.odl $(PKGDIR)/etc/amx/$(COMPONENT)/deviceinfo_prefix.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_cert_path.odl $(PKGDIR)/etc/amx/$(COMPONENT)/deviceinfo_cert_path.odl
	$(INSTALL) -D -p -m 0644 odl/deviceinfo_vendorlogfile_1.odl $(PKGDIR)/etc/amx/$(COMPONENT)/deviceinfo_vendorlogfile_1.odl
	$(INSTALL) -D -p -m 0644 system/odl/deviceinfo-system.odl $(PKGDIR)/etc/amx/deviceinfo-system/deviceinfo-system.odl
	$(INSTALL) -D -p -m 0644 system/odl/deviceinfo-system_definition.odl $(PKGDIR)/etc/amx/deviceinfo-system/deviceinfo-system_definition.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/untrusted/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/untrusted/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/deviceinfo-system
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/deviceinfo-system
ifeq ($(CONFIG_SAH_AMX_TR181_DEVICEINFO_DEVELOPMENT),y)
	$(INSTALL) -D -p -m 0755 scripts/00_core_dump_limit.sh $(PKGDIR)/etc/env.d/00_core_dump_limit.sh
endif
	$(INSTALL) -D -p -m 0755 scripts/00_environment_deviceinfo.sh $(PKGDIR)/etc/env.d/00_environment_deviceinfo.sh
ifeq ($(CONFIG_ACCESSPOINT),y)
ifneq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_ap_hostname.sh $(PKGDIR)/etc/env.d/00_ap_hostname.sh
endif
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_gw_hostname.sh $(PKGDIR)/etc/env.d/00_gw_hostname.sh
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
ifneq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_ap_friendlyname.sh $(PKGDIR)/etc/env.d/00_ap_friendlyname.sh
endif
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/00_gw_friendlyname.sh $(PKGDIR)/etc/env.d/00_gw_friendlyname.sh
endif
	$(INSTALL) -D -p -m 0755 scripts/00_environment_mfg_helper.sh $(PKGDIR)/etc/env.d/00_environment_mfg_helper.sh
	$(INSTALL) -D -p -m 0755 scripts/01_calculate_mac_addresses_from_basemacaddress.sh $(PKGDIR)/etc/env.d/01_calculate_mac_addresses_from_basemacaddress.sh
	$(INSTALL) -D -p -m 0755 scripts/00_environment_xclone_macaddres.sh $(PKGDIR)/etc/env.d/00_environment_xclone_macaddres.sh
	$(INSTALL) -D -p -m 0755 scripts/01_calculate_mac_addresses_from_basemacaddress.sh $(PKGDIR)/etc/env.d/01_calculate_mac_addresses_from_basemacaddress.sh
	$(INSTALL) -D -p -m 0755 scripts/environment.sh $(PKGDIR)$(INITDIR)/deviceinfo-environment
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc
	rm -f $(PKGDIR)/etc/environment
	ln -sfr $(PKGDIR)/var/etc/environment $(PKGDIR)/etc/environment
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/deviceinfo-system.sh $(PKGDIR)$(INITDIR)/deviceinfo-system
ifeq ($(CONFIG_SAH_AMX_ADD_SYSINIT_DONE_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/sysinit_done.sh $(PKGDIR)$(INITDIR)/z_sysinit_done
endif
ifeq ($(CONFIG_SAH_AMX_ADD_DBREADY_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/dbready.sh $(PKGDIR)$(INITDIR)/w_dbready
endif
ifeq ($(CONFIG_SAH_AMX_ADD_DBREADY_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/software_version_hash.sh $(PKGDIR)/usr/sbin/software_version_hash.sh
endif
	$(INSTALL) -D -p -m 0755 scripts/debug_lcm_containers.sh $(PKGDIR)/usr/lib/debuginfo/debug_lcm_containers.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_memory.sh $(PKGDIR)/usr/lib/debuginfo/debug_memory.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_process.sh $(PKGDIR)/usr/lib/debuginfo/debug_process.sh
ifeq ($(CONFIG_SAH_AMX_ADD_DBREADY_SCRIPT),y)
	$(INSTALL) -D -p -m 0755 scripts/trigger_restore.sh $(PKGDIR)/lib/preinit/98_trigger_restore
endif
	$(INSTALL) -D -p -m 0755 scripts/logrotate_add.sh $(PKGDIR)/usr/lib/debuginfo/logrotate_add.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_remove.sh $(PKGDIR)/usr/lib/debuginfo/logrotate_remove.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_mv.sh $(PKGDIR)/usr/lib/debuginfo/logrotate_mv.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_append.sh $(PKGDIR)/usr/lib/debuginfo/logrotate_append.sh
	$(INSTALL) -D -p -m 0755 scripts/logrotate_var.sh $(PKGDIR)/usr/lib/debuginfo/logrotate_var.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_logrotate.sh $(PKGDIR)/usr/lib/debuginfo/debug_logrotate.sh
	$(INSTALL) -D -p -m 0755 scripts/tmpfs_download_var.sh $(PKGDIR)/usr/lib/amx/scripts/tmpfs_download_var.sh
	$(INSTALL) -D -p -m 0755 scripts/tmpfs_download_mount.sh $(PKGDIR)/usr/lib/amx/scripts/tmpfs_download_mount.sh
	$(INSTALL) -D -p -m 0755 scripts/tmpfs_download_unmount.sh $(PKGDIR)/usr/lib/amx/scripts/tmpfs_download_unmount.sh
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-lr-logrotate.so $(PKGDIR)/usr/lib/amx/modules/mod-lr-logrotate.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-syslog.so $(PKGDIR)/usr/lib/amx/modules/mod-syslog.so
ifeq ($(CONFIG_SAH_AMX_TR181_DEVICEINFO_REQUIRES_USERS),y)
	$(INSTALL) -D -p -m 0644 odl/extensions/$(COMPONENT)_requires_users.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/$(COMPONENT)_requires_users.odl
endif
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/$(COMPONENT).odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-lr-logrotate/test run
	$(MAKE) -C mod-lr-logrotate/test coverage
	$(MAKE) -C mod-syslog/test run
	$(MAKE) -C mod-syslog/test coverage
	$(MAKE) -C system/test run
	$(MAKE) -C system/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test