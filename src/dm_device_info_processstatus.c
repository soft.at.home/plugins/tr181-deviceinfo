/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <debug/sahtrace.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>

#include "deviceinfo.h"
/**********************************************************
* Macro definitions
**********************************************************/
#define NUMBER_OF_FIELDS_EXTRACTED_FOR_EACH_PID 6
/**********************************************************
* Function Prototypes
**********************************************************/
int process_info_read(amxc_var_t* data);
int is_pid_folder(const struct dirent* entry);

amxd_status_t get_process(const char* entry_name, amxc_var_t* process);
amxd_status_t iterate_over_processes(DIR* procdir, amxc_var_t* process_list);
amxd_status_t _process_status_cleanup(amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv);

/**********************************************************
* Functions
**********************************************************/
int is_pid_folder(const struct dirent* entry) {
    const char* p;

    for(p = entry->d_name; *p; p++) {
        if(!isdigit(*p)) {
            return 0;
        }
    }
    return 1;
}

int process_info_read(amxc_var_t* data) {
    int retval = -1;
    DIR* procdir = NULL;

    // Open /proc directory.
    procdir = opendir("/proc");
    if(procdir == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open /proc dir");
        goto exit;
    }

    iterate_over_processes(procdir, data);

    retval = 0;

exit:
    if(procdir != NULL) {
        if(closedir(procdir) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to close /proc dir");
        }
    }
    return retval;
}

amxd_status_t iterate_over_processes(DIR* procdir, amxc_var_t* process_list) {

    amxd_status_t status = amxd_status_ok;
    struct dirent* entry;

    amxc_var_set_type(process_list, AMXC_VAR_ID_LIST);

    while((entry = readdir(procdir))) {
        // Skip anything that is not a PID folder.
        if(!is_pid_folder(entry)) {
            continue;
        }

        //Create variant for one process
        amxc_var_add(cstring_t, process_list, entry->d_name);
    }

    return status;
}


amxd_status_t get_process(const char* entry_name, amxc_var_t* process) {

    amxd_status_t status = amxd_status_ok;
    int pid = 0;
    char state[256] = {0};
    char command[300] = {0};
    char temp[300] = {0};
    FILE* fp = NULL;
    unsigned long vsize = 0;
    unsigned long priority = 0;
    unsigned long cpu_time = 0;
    //Create variant
    amxc_var_set_type(process, AMXC_VAR_ID_HTABLE);

    // Try to open /proc/<PID>/stat.
    snprintf(command, sizeof(command), "/proc/%s/stat", entry_name);
    fp = fopen(command, "r");

    if(fp == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open /proc/<PID>/stat");
        amxc_var_add_key(cstring_t, process, "Command", "");
        amxc_var_add_key(uint32_t, process, "Size", 0);
        amxc_var_add_key(uint32_t, process, "Priority", 0);
        amxc_var_add_key(uint32_t, process, "CPUTime", 0);
        amxc_var_add_key(cstring_t, process, "State", "Stopped");
        goto exit;
    }

    // Get the fields needed for TR181
    int ret = fscanf(fp, "%d    %s    %c    %*d  %*d   %*d   %*d   %*d   %*u   %*u \
                          %*u   %*u   %*u   %lu  %*u   %*d   %*d   %ld   %*d   %*d \
                          %*d   %*d   %lu",
                     &pid, command, state, &cpu_time, &priority, &vsize
                     );
    if(ret != NUMBER_OF_FIELDS_EXTRACTED_FOR_EACH_PID) {
        SAH_TRACEZ_ERROR(ME, "Failed to extract info about PID");
        goto exit;
    }
    amxc_var_add_key(uint32_t, process, "PID", pid);
    // Format name of the command - brackets removed
    strncpy(temp, command + 1, strlen(command) - 2);
    amxc_var_add_key(cstring_t, process, "Command", temp);
    amxc_var_add_key(uint32_t, process, "Size", vsize / 1024);
    amxc_var_add_key(uint32_t, process, "Priority", priority);
    amxc_var_add_key(uint32_t, process, "CPUTime", cpu_time);
    amxc_var_add_key(cstring_t, process, "State", &state[0]); // state is not correct according to TR181

exit:
    if(fp != NULL) {
        if(fclose(fp) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to close /proc/<PID>/stat");
        }
    }
    return status;
}

amxd_status_t _process_status_cleanup(amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    amxp_timer_t* timer = NULL;

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    timer = (amxp_timer_t*) object->priv;
    amxp_timer_delete(&timer);
    object->priv = NULL;

exit:
    return status;
}