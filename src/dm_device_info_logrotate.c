/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo_logrotate.h"

#define LOGROTATE_PATH "DeviceInfo.LogRotate."

int dm_update_logrotate(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* root_obj = deviceinfo_get_root();
    int retval = -1;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_null(args, exit);
    when_null_trace(root_obj, exit, ERROR, "Could not get DeviceInfo object");

    amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, args)) {
        amxc_var_t* parameters = amxc_var_from_llist_it(it);
        const char* logrotate_alias = GET_CHAR(parameters, "SyslogActionAlias");
        const char* action = GET_CHAR(parameters, "Action");

        when_null_trace(logrotate_alias, exit, ERROR, "Unknown alias");
        if(strcmp(action, "add") == 0) {
            amxc_var_t enable;
            if(amxd_object_findf(root_obj, "LogRotate.[Alias == \"%s\"].", logrotate_alias) == NULL) {
                // New file, add instance
                amxd_trans_select_pathf(&trans, LOGROTATE_PATH);
                amxd_trans_add_inst(&trans, 0, NULL);
                amxd_trans_set_param(&trans, "Alias", GET_ARG(parameters, "SyslogActionAlias"));
            } else {
                // Update existing instance
                amxd_trans_select_pathf(&trans, LOGROTATE_PATH "[Alias == \"%s\"].", logrotate_alias);
            }

            amxc_var_init(&enable);
            amxc_var_set(bool, &enable, true);
            amxd_trans_set_param(&trans, "Enable", &enable);
            amxd_trans_set_param(&trans, "Name", GET_ARG(parameters, "SyslogActionLogFilePath"));
            amxc_var_clean(&enable);
        } else if(strcmp(action, "update") == 0) {
            when_failed_trace(amxd_trans_select_pathf(&trans, LOGROTATE_PATH "[Alias == \"%s\"].", logrotate_alias), exit, ERROR, "Could not select LogRotate for update");
            amxd_trans_set_param(&trans, "Name", GET_ARG(parameters, "SyslogActionLogFilePath"));
        } else if(strcmp(action, "remove") == 0) {
            amxd_trans_select_pathf(&trans, LOGROTATE_PATH);
            amxd_trans_del_inst(&trans, 0, amxd_object_get_name(amxd_object_findf(root_obj, "LogRotate.[Alias == \"%s\"].", logrotate_alias), AMXD_OBJECT_NAMED));
        }
    }

    retval = amxd_trans_apply(&trans, deviceinfo_get_dm());
    when_failed_trace(retval, exit, ERROR, "Failed to update LogRotate entries retval=%d", retval);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int dm_update_logrotate_logfile(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_null(args, exit);

    amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, args)) {
        amxc_var_t* file_data = amxc_var_from_llist_it(it);
        amxd_object_t* parent_obj = NULL;
        const char* logrotate_alias = GET_CHAR(file_data, "ParentAlias");
        const char* action = GET_CHAR(file_data, "Action");

        parent_obj = amxd_object_findf(deviceinfo_get_root(), "LogRotate.[Alias == \"%s\"].", logrotate_alias);
        when_null_trace(parent_obj, exit, ERROR, "Could not find LogRotate object with Alias '%s'", logrotate_alias);

        if(strcmp(action, "add") == 0) {
            if(amxd_object_findf(parent_obj, "LogFile.[Name == \"%s\"].", GET_CHAR(file_data, "Name")) == 0) {
                // New file, add instance
                amxd_trans_select_pathf(&trans, LOGROTATE_PATH "[Alias == \"%s\"].LogFile.", logrotate_alias);
                amxd_trans_add_inst(&trans, 0, NULL);
            } else {
                // Update existing instance
                amxd_trans_select_pathf(&trans, LOGROTATE_PATH "[Alias == \"%s\"].LogFile.[Name == \"%s\"].", logrotate_alias, GET_CHAR(file_data, "Name"));
            }

            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_set_param(&trans, "Name", GET_ARG(file_data, "Name"));
            amxd_trans_set_param(&trans, "Size", GET_ARG(file_data, "Size"));
            amxd_trans_set_param(&trans, "LastChange", GET_ARG(file_data, "LastChange"));
        } else if(strcmp(action, "remove") == 0) {
            amxd_trans_select_pathf(&trans, LOGROTATE_PATH "[Alias == \"%s\"].LogFile.", logrotate_alias);
            amxd_trans_del_inst(&trans, 0, amxd_object_get_name(amxd_object_findf(parent_obj, "LogFile.[Name == \"%s\"].", GET_CHAR(file_data, "Name")), AMXD_OBJECT_NAMED));
        }
    }

    retval = amxd_trans_apply(&trans, deviceinfo_get_dm());
    when_failed_trace(retval, exit, ERROR, "Failed to update LogRotate LogFile entries retval=%d", retval);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int set_logrotate_interval(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t deviceinfo_parameters;
    amxd_object_t* root_object = deviceinfo_get_root();
    int retval = -1;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_init(&deviceinfo_parameters);

    when_null(root_object, exit);

    amxd_object_get_params(root_object, &deviceinfo_parameters, amxd_dm_access_protected);

    amxc_var_set(uint32_t, &args, GET_UINT32(&deviceinfo_parameters, "LogRotateInterval"));

    retval = amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "change-lr-interval", &args, &ret);
    when_failed_trace(retval, exit, ERROR, "Failed to set logrotate interval with the module");

exit:
    amxc_var_clean(&deviceinfo_parameters);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

char* get_filepath(const char* name) {
    SAH_TRACEZ_IN(ME);
    char* result = NULL;
    int len = 0;

    when_null_trace(name, exit, ERROR, "The name is NULL");

    if(strncmp(name, "file://", strlen("file://")) == 0) {
        len = strlen("file://");
    } else if(strncmp(name, "file:", strlen("file:")) == 0) {
        len = strlen("file:");
    }

    result = malloc(strlen(name) - len + 1);
    when_null(result, exit);
    memcpy(result, name + len, strlen(name) - len);
    result[strlen(name) - len] = '\0';

exit:
    SAH_TRACEZ_OUT(ME);
    return result;
}

void _log_rotate_added(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* name_key = NULL;
    char* filepath = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    when_null(data, exit);

    amxc_var_copy(&args, GET_ARG(data, "parameters"));

    filepath = get_filepath(GET_CHAR(&args, "Name"));
    name_key = amxc_var_get_key(&args, "Name", AMXC_VAR_FLAG_DEFAULT);
    when_null(name_key, exit);
    amxc_var_set(cstring_t, name_key, filepath);

    if(GET_BOOL(&args, "Enable") && (amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "add-lr-entry", &args, &ret) != 0)) {
        SAH_TRACEZ_ERROR(ME, "Failed to add logrotate entry with the module");
    }

exit:
    free(filepath);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

void _log_rotate_interval_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* var = NULL;
    amxc_var_t* changed_parameters = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    when_null(data, exit);

    changed_parameters = GET_ARG(data, "parameters");
    when_null(changed_parameters, exit);

    var = GET_ARG(changed_parameters, "LogRotateInterval");
    when_null(var, exit);

    amxc_var_set(uint32_t, &args, GET_UINT32(var, "to"));

    if(amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "change-lr-interval", &args, &ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set logrotate interval with the module");
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

void _log_rotate_enable_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* var = NULL;
    amxc_var_t* changed_parameters;
    amxd_object_t* object = NULL;
    bool enable = false;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    when_null(data, exit);

    object = amxd_dm_signal_get_object(deviceinfo_get_dm(), data);
    when_null_trace(object, exit, ERROR, "Object is NULL");

    changed_parameters = GET_ARG(data, "parameters");
    when_null(changed_parameters, exit);

    var = GET_ARG(changed_parameters, "Enable");
    when_null(var, exit);

    amxd_object_get_params(object, &args, amxd_dm_access_protected);
    enable = GET_BOOL(var, "to");
    if(enable) {
        amxc_var_t* name_key = amxc_var_get_key(&args, "Name", AMXC_VAR_FLAG_DEFAULT);
        char* filepath = get_filepath(GET_CHAR(&args, "Name"));

        if(name_key == NULL) {
            free(filepath);
            goto exit;
        }

        amxc_var_set(cstring_t, name_key, filepath);

        if(amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "add-lr-entry", &args, &ret) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to add logrotate entry with the module");
        }
        free(filepath);
    } else {
        if(amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "delete-lr-entry", &args, &ret) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to delete logrotate entry with the module");
        }
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

void _log_rotate_changed(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t object_parameters;
    amxc_var_t* var = NULL;
    amxc_var_t* changed_parameters = NULL;
    amxd_object_t* object = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_init(&object_parameters);

    when_null(data, exit);

    object = amxd_dm_signal_get_object(deviceinfo_get_dm(), data);
    when_null(object, exit);

    amxd_object_get_params(object, &object_parameters, amxd_dm_access_protected);
    when_false(GET_BOOL(&object_parameters, "Enable"), exit);

    changed_parameters = GET_ARG(data, "parameters");
    when_null(changed_parameters, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", GET_CHAR(&object_parameters, "Alias"));

    var = GET_ARG(changed_parameters, "Name");
    if(var != NULL) {
        char* filepath = get_filepath(GET_CHAR(var, "to"));
        amxc_var_add_key(cstring_t, &args, "Name", filepath);
        free(filepath);
    }
    var = GET_ARG(changed_parameters, "NumberOfFiles");
    if(var != NULL) {
        amxc_var_add_key(uint32_t, &args, "NumberOfFiles", GET_UINT32(var, "to"));
    }
    var = GET_ARG(changed_parameters, "MaxFileSize");
    if(var != NULL) {
        amxc_var_add_key(uint32_t, &args, "MaxFileSize", GET_UINT32(var, "to"));
    }
    var = GET_ARG(changed_parameters, "RollOver");
    if(var != NULL) {
        amxc_var_add_key(uint32_t, &args, "RollOver", GET_UINT32(var, "to"));
    }
    var = GET_ARG(changed_parameters, "Retention");
    if(var != NULL) {
        amxc_var_add_key(uint32_t, &args, "Retention", GET_UINT32(var, "to"));
    }
    var = GET_ARG(changed_parameters, "Compression");
    if(var != NULL) {
        amxc_var_add_key(cstring_t, &args, "Compression", GET_CHAR(var, "to"));
    }
    var = GET_ARG(changed_parameters, "DelayCompression");
    if(var != NULL) {
        amxc_var_add_key(bool, &args, "DelayCompression", GET_BOOL(var, "to"));
    }
    var = GET_ARG(changed_parameters, "CopyTruncate");
    if(var != NULL) {
        amxc_var_add_key(bool, &args, "CopyTruncate", GET_BOOL(var, "to"));
    }
    var = GET_ARG(changed_parameters, "SuUser");
    if(var != NULL) {
        amxc_var_add_key(cstring_t, &args, "SuUser", GET_CHAR(var, "to"));
    }
    var = GET_ARG(changed_parameters, "SuGroup");
    if(var != NULL) {
        amxc_var_add_key(cstring_t, &args, "SuGroup", GET_CHAR(var, "to"));
    }

    if(amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "change-lr-entry", &args, &ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to change logrotate entry with the module");
    }

exit:
    amxc_var_clean(&object_parameters);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t _log_rotate_removed(amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  amxd_action_t reason,
                                  UNUSED const amxc_var_t* const data,
                                  UNUSED amxc_var_t* const retval,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_action;
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    when_false_trace(reason == action_object_destroy, exit, ERROR, "Wrong reason: got %d, expected %d", reason, action_object_destroy);
    rv = amxd_status_invalid_function_argument;
    when_null_trace(object, exit, ERROR, "Object is null");
    when_false_status(object->type == amxd_object_instance, exit, rv = amxd_status_ok);

    amxd_object_get_params(object, &args, amxd_dm_access_protected);
    rv = amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "delete-lr-entry", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to remove logrotate entry with the module rv=%d", rv);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _UpdateLogRotate(UNUSED amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* data = NULL;
    const char* mod_name = NULL;
    const char* mod_namespace = NULL;
    const char* func_name = NULL;
    int retval = 1;
    amxd_status_t stat = amxd_status_invalid_function_argument;

    mod_name = GET_CHAR(args, "mod_name");
    mod_namespace = GET_CHAR(args, "mod_namespace");
    func_name = GET_CHAR(args, "fnc");
    data = GET_ARG(args, "data");

    when_null_trace(mod_name, exit, ERROR, "Mod name is null");
    when_null_trace(mod_namespace, exit, ERROR, "Mod namespace is null");
    when_null_trace(func_name, exit, ERROR, "Function name is null");
    when_null_trace(data, exit, ERROR, "Data is null");

    stat = amxd_status_unknown_error;
    retval = amxm_execute_function(mod_name, mod_namespace, func_name, data, ret);
    SAH_TRACEZ_INFO(ME, "Mod %s, func '%s' returned %d", mod_name, func_name, retval);

    if(retval == 0) {
        stat = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return stat;
}
