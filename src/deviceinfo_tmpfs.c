/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo.h"
#include "deviceinfo_tmpfs.h"

#define STDOUT_BUFFER_SIZE 9128

static bool tmpfs_initialized = false;

static uint32_t get_uint32_param(const char* parameter, bool with_prefix) {
    amxd_object_t* deviceinfo = amxd_dm_findf(deviceinfo_get_dm(), "DeviceInfo.");
    amxc_string_t param;
    uint32_t value = 0;

    amxc_string_init(&param, 0);

    when_null(deviceinfo, exit);
    amxc_string_setf(&param, "%s%s", with_prefix ? vendor_prefix() : "", parameter);
    value = GET_UINT32(amxd_object_get_param_value(deviceinfo, amxc_string_get(&param, 0)), NULL);

exit:
    amxc_string_clean(&param);
    return value;
}

static void read_from_fd(const int fd, amxc_string_t* string) {
    SAH_TRACEZ_IN(ME);
    char buffer[STDOUT_BUFFER_SIZE];
    ssize_t bytes = 0;

    when_null_trace(string, exit, ERROR, "Output string is not initialised.");
    when_false_trace(fd > 0, exit, ERROR, "File descriptor is not bigger than 0.");

    bytes = read(fd, buffer, STDOUT_BUFFER_SIZE);
    when_true(((bytes <= 0) || (bytes >= STDOUT_BUFFER_SIZE)), exit);

    if(buffer[0] != '\0') {
        buffer[bytes] = 0;
        amxc_string_append(string, buffer, bytes);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

static int execute_tmpfs_script_get_output(const cstring_t script, int timeout_ms, amxc_string_t* output,
                                           const cstring_t p1, const cstring_t p2, const cstring_t p3, const cstring_t p4, const cstring_t p5) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxp_subproc_t* proc = NULL;
    int32_t fd_stdout = -1;
    int32_t fd_stderr = -1;
    amxc_llist_t param_list;
    int ret_int = -1;
    int counter = 0;
    bool file_locked = false;

    amxc_llist_init(&param_list);

    when_null_trace(output, exit, ERROR, "Output string is NULL");
    when_str_empty_trace(script, exit, ERROR, "Script path is an empty string or NULL.");
    when_failed_trace(access(script, F_OK), exit, ERROR, "Script '%s' doesn't exist.", script);

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd_stdout = amxp_subproc_open_fd(proc, STDOUT_FILENO);
    when_false_trace(fd_stdout > 0, exit, ERROR, "Failed to open file descriptor.");

    fd_stderr = amxp_subproc_open_fd(proc, STDERR_FILENO);
    when_false_trace(fd_stderr > 0, exit, ERROR, "Failed to open file descriptor.");

    if(access(TMPFS_SCRIPT_LOCK_FILE, F_OK) == 0) {
        file_locked = true;
    }

    rv = amxp_subproc_start(proc, (char*) "sh", script, p1, p2, p3, p4, p5, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    do {
        read_from_fd(fd_stdout, output);
        read_from_fd(fd_stderr, output);
        if(counter > timeout_ms) {
            SAH_TRACEZ_ERROR(ME, "Timeout reached to execute the script %s", script);
            break;
        }
        counter++;
    } while (amxp_subproc_wait(proc, 1) == 1);
    ret_int = amxp_subproc_get_exitstatus(proc);
    amxp_subproc_kill(proc, SIGKILL);

    if(!file_locked && (access(TMPFS_SCRIPT_LOCK_FILE, F_OK) == 0)) {
        remove(TMPFS_SCRIPT_LOCK_FILE);
    }

exit:
    amxp_subproc_delete(&proc);
    amxc_llist_clean(&param_list, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return ret_int;
}

void tmpfs_partition_init(void) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* string_stdout = NULL;
    amxc_string_t file_size_str;
    amxc_string_t param_size_str;
    amxc_string_t param_duration_str;
    int ret_status = -1;
    const char* param_path = download_tmpfs_partition();
    uint32_t param_size = download_tmpfs_size_mb();
    uint32_t param_duration = get_uint32_param(TMPFS_DOWNLOAD_DURATION_PARAM, true);

    amxc_string_new(&string_stdout, 0);
    amxc_string_init(&file_size_str, 0);
    amxc_string_init(&param_size_str, 0);
    amxc_string_init(&param_duration_str, 0);

    SAH_TRACEZ_WARNING(ME, "Initializing tmpfs partition");

    when_str_empty_trace(param_path, exit, ERROR, "download_tmpfs_partition is empty string.");
    when_false_trace(param_size > 0, exit, ERROR, "download_tmpfs_size_mb is 0.");
    when_false_trace(param_duration > 0, exit, ERROR, TMPFS_DOWNLOAD_DURATION_PARAM " is 0.");
    when_true(tmpfs_initialized, exit);

    amxc_string_setf(&param_size_str, "%d", param_size);
    amxc_string_setf(&param_duration_str, "%d", param_duration);
    amxc_string_setf(&file_size_str, "%d", get_uint32_param(TMPFS_DOWNLOAD_SIZE_PARAM, true));

    ret_status = execute_tmpfs_script_get_output(TMPFS_SCRIPT_UNMOUNT_PATH, 5000, string_stdout, "root", param_path, amxc_string_get(&param_size_str, 0), amxc_string_get(&param_duration_str, 0), NULL);
    if(ret_status != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute script '"TMPFS_SCRIPT_UNMOUNT_PATH "'. Output from script: %s", amxc_string_get(string_stdout, 0));
    }

    amxc_string_reset(string_stdout);
    ret_status = execute_tmpfs_script_get_output(TMPFS_SCRIPT_MOUNT_PATH, 5000, string_stdout, TMPFS_SCRIPT_USER, amxc_string_get(&file_size_str, 0), param_path, amxc_string_get(&param_size_str, 0), "0");
    when_failed_trace(ret_status, exit, ERROR, "Failed to execute script '"TMPFS_SCRIPT_MOUNT_PATH "'. Output from script: %s", amxc_string_get(string_stdout, 0));

    tmpfs_initialized = true;

exit:
    amxc_string_delete(&string_stdout);
    amxc_string_clean(&file_size_str);
    amxc_string_clean(&param_size_str);
    amxc_string_clean(&param_duration_str);
    SAH_TRACEZ_OUT(ME);
    return;
}
