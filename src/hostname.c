/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#include "deviceinfo.h"
#include "deviceinfo_dm_mngr.h"

#undef ME
#define ME "devinfo-hn"

static bool waiting_for_dns = false;

static bool set_dns_hosts(void) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxc_string_t search_path;
    amxc_var_t values;
    amxc_var_t ret;
    const char* hostname = NULL;
    int rv = -1;

    amxc_var_init(&values);
    amxc_var_init(&ret);
    amxc_string_init(&search_path, 0);

    hostname = GET_CHAR(amxd_object_get_param_value(deviceinfo_get_root(), "HostName"), NULL);
    when_str_empty_trace(hostname, exit, ERROR, "HostName empty");

    bus_ctx = amxb_be_who_has("DNS.");
    when_null_trace(bus_ctx, exit, ERROR, "Failed to get DNS. bus ctx");

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", hostname);

    // update the Name parameter of all instances that match the search path

    amxc_string_setf(&search_path, "DNS.%sHost.[Alias matches '^deviceinfo-[0-9a-zA-Z]*$'].", vendor_prefix());
    rv = amxb_set(bus_ctx, amxc_string_get(&search_path, 0), &values, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to update DNS datamodel %d", rv);

    SAH_TRACEZ_INFO(ME, "updated DNS datamodel");

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    amxc_string_clean(&search_path);
    return rv == 0;
}

static void wait_done_cb(UNUSED const char* const signal,
                         UNUSED const amxc_var_t* const date,
                         UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "wait:done received");

    amxp_slot_disconnect(NULL, "wait:done", wait_done_cb);
    waiting_for_dns = false;

    set_dns_hosts();
}

static void set_dns_hosts_when_available(void) {
    SAH_TRACEZ_INFO(ME, "wait for DNS objects");
    amxp_slot_connect(NULL, "wait:done", NULL, wait_done_cb, NULL);
    amxb_wait_for_object("DNS."); // works async
    waiting_for_dns = true;
}

void _hostname_changed(UNUSED const char* const sig_name,
                       UNUSED const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    const char* hostname = NULL;
    SAH_TRACEZ_IN(ME);

    hostname = GET_CHAR(amxd_object_get_param_value(deviceinfo_get_root(), "HostName"), NULL);
    when_str_empty_trace(hostname, exit, ERROR, "HostName empty");

    when_true(waiting_for_dns, exit);
    if(amxb_be_who_has("DNS.") != NULL) {
        set_dns_hosts();
    } else {
        set_dns_hosts_when_available();
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}