/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "test_module.h"
#include "../common/mock.h"
#include "mod_logrotate.h"
#include "logrotate_list.h"
#include "logrotate_cron.h"
#include "logrotate_utils.h"

#define DEFAULT_COMPRESSION "BZIP2"
#define DEFAULT_NAME "/var/log/messages"
#define DEFAULT_MAX_FILE_SIZE (5000)
#define DEFAULT_NR_FILES (5)
#define DEFAULT_RETENTION (7)
#define DEFAULT_ROLL_OVER (3)

uint32_t expected_system_calls = 0;
uint32_t expected_removes = 0;
uint32_t expected_write_to_file = 0;
uint32_t total_system_calls = 0;
uint32_t total_removes = 0;
uint32_t total_write_to_file = 0;
bool compress = true;
bool delay_compress = true;
bool copy_truncate = false;
bool su_used = false;

static void assert_calls(void) {
    assert_int_equal(total_system_calls, expected_system_calls);
    assert_int_equal(total_removes, expected_removes);
    assert_int_equal(total_write_to_file, expected_write_to_file);
}

static void set_default_values(amxc_var_t* data, const char* alias) {
    amxc_var_add_key(cstring_t, data, "Alias", alias);
    amxc_var_add_key(cstring_t, data, "Compression", DEFAULT_COMPRESSION);
    amxc_var_add_key(uint32_t, data, "MaxFileSize", DEFAULT_MAX_FILE_SIZE);
    amxc_var_add_key(cstring_t, data, "Name", DEFAULT_NAME);
    amxc_var_add_key(uint32_t, data, "NumberOfFiles", DEFAULT_NR_FILES);
    amxc_var_add_key(uint32_t, data, "Retention", DEFAULT_RETENTION);
    amxc_var_add_key(uint32_t, data, "RollOver", DEFAULT_ROLL_OVER);
}

int test_setup(void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxut_bus_setup(state);
    amxut_bus_handle_events();
    assert_calls();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "add", "/usr/lib/debuginfo/logrotate_add.sh");
    amxc_var_add_key(cstring_t, &args, "remove", "/usr/lib/debuginfo/logrotate_remove.sh");

    mod_set_scripts(NULL, &args, NULL);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return 0;
}

int test_teardown(void** state) {
    amxm_close_all();
    amxut_bus_handle_events();
    amxut_bus_teardown(state);

    return 0;
}

void test_set_interval(UNUSED void** state) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set(uint32_t, &data, 12);

    assert_int_equal(mod_change_logrotate_interval(NULL, &data, NULL), 0);
    amxut_bus_handle_events();
    expected_write_to_file += 1; // Add crontab config
    expected_system_calls += 1;  // Adding crontabs
    assert_calls();              // No calls done, only DM updates

    assert_int_equal(logrotate_list_size(), 0);

    amxc_var_clean(&data);
}

void test_add_entries(UNUSED void** state) {
    amxc_var_t messages;
    amxc_var_t log;
    amxc_var_t firewall;
    amxc_var_t log2;
    amxc_var_t* log_compression = NULL;
    amxc_var_t* firewall_compression = NULL;
    amxc_var_t* log2_compression = NULL;

    amxc_var_init(&messages);
    amxc_var_init(&log);
    amxc_var_init(&firewall);
    amxc_var_init(&log2);

    amxc_var_set_type(&messages, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&log, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&firewall, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&log2, AMXC_VAR_ID_HTABLE);
    set_default_values(&messages, "messages");
    set_default_values(&log, "log");
    set_default_values(&firewall, "firewall");
    set_default_values(&log2, "log2");

    log_compression = amxc_var_get_key(&log, "Compression", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, log_compression, "ZSTD");
    firewall_compression = amxc_var_get_key(&firewall, "Compression", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, firewall_compression, "GZIP");
    log2_compression = amxc_var_get_key(&log2, "Compression", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, log2_compression, "XZ");

    delay_compress = false;
    assert_int_equal(mod_add_logrotate_entry(NULL, &messages, NULL), 0);
    amxut_bus_handle_events();
    expected_write_to_file += 2; // Add logrotate config file + crontab config
    expected_removes += 1;       // Remove crontab config
    expected_system_calls += 2;  // Remove + add crontabs
    assert_calls();

    assert_int_equal(mod_add_logrotate_entry(NULL, &log, NULL), 0);
    amxut_bus_handle_events();
    expected_write_to_file += 2; // Add logrotate config file + crontab config
    expected_removes += 1;       // Remove crontab config
    expected_system_calls += 2;  // Remove + add crontabs
    assert_calls();

    assert_int_equal(mod_add_logrotate_entry(NULL, &firewall, NULL), 0);
    amxut_bus_handle_events();
    expected_write_to_file += 2; // Add logrotate config file + crontab config
    expected_removes += 1;       // Remove crontab config
    expected_system_calls += 2;  // Remove + add crontabs
    assert_calls();

    assert_int_equal(mod_add_logrotate_entry(NULL, &log2, NULL), 0);
    amxut_bus_handle_events();
    expected_write_to_file += 2; // Add logrotate config file + crontab config
    expected_removes += 1;       // Remove crontab config
    expected_system_calls += 2;  // Remove + add crontabs
    assert_calls();

    assert_int_equal(logrotate_list_size(), 4);

    amxc_var_clean(&log2);
    amxc_var_clean(&firewall);
    amxc_var_clean(&log);
    amxc_var_clean(&messages);
}

void test_change_entries(UNUSED void** state) {
    amxc_var_t messages;
    amxc_var_t log;
    amxc_var_t firewall;
    amxc_var_t log2;

    amxc_var_init(&messages);
    amxc_var_init(&log);
    amxc_var_init(&firewall);
    amxc_var_init(&log2);

    amxc_var_set_type(&messages, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&log, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&firewall, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&log2, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, &messages, "MaxFileSize", DEFAULT_MAX_FILE_SIZE + 1);
    amxc_var_add_key(uint32_t, &messages, "NumberOfFiles", DEFAULT_NR_FILES + 2);
    amxc_var_add_key(cstring_t, &messages, "Compression", "None");
    amxc_var_add_key(bool, &messages, "DelayCompression", true);
    amxc_var_add_key(bool, &messages, "CopyTruncate", true);
    delay_compress = false; // Not writing any compression config since compression is "None"
    copy_truncate = true;
    compress = false;

    assert_int_equal(mod_change_logrotate_entry(NULL, &messages, NULL), -1); // Missing alias
    amxut_bus_handle_events();

    amxc_var_add_key(cstring_t, &messages, "Alias", "messages");
    assert_int_equal(mod_change_logrotate_entry(NULL, &messages, NULL), 0);
    amxut_bus_handle_events();
    expected_removes += 1;       // Remove logrotate config file
    expected_write_to_file += 1; // Add logrotate config file
    assert_calls();

    compress = true;
    delay_compress = true;
    copy_truncate = false;
    su_used = true;

    amxc_var_add_key(cstring_t, &log, "Alias", "log");
    amxc_var_add_key(uint32_t, &log, "Retention", 0);
    amxc_var_add_key(uint32_t, &log, "RollOver", DEFAULT_ROLL_OVER + 3);
    amxc_var_add_key(bool, &log, "DelayCompression", true);
    amxc_var_add_key(cstring_t, &log, "SuUser", "root");
    amxc_var_add_key(cstring_t, &log, "SuGroup", "root");

    assert_int_equal(mod_change_logrotate_entry(NULL, &log, NULL), 0);
    amxut_bus_handle_events();
    expected_write_to_file += 2; // Add logrotate config file + crontab
    expected_removes += 2;       // Remove logrotate config file + remove crontab config
    expected_system_calls += 2;  // Remove old crontabs + add 1 crontab for new settings
    assert_calls();

    su_used = false;

    amxc_var_add_key(cstring_t, &firewall, "Alias", "firewall");
    amxc_var_add_key(cstring_t, &firewall, "Compression", "InvalidCompression");

    // InvalidCompression is not valid so function should fail on this.
    assert_int_equal(mod_change_logrotate_entry(NULL, &firewall, NULL), -1);
    amxut_bus_handle_events();
    assert_calls(); // Change failed so no calls should have happened

    amxc_var_add_key(cstring_t, &log2, "Alias", "log2");
    amxc_var_add_key(cstring_t, &log2, "Name", "/var/log/important.log");

    delay_compress = false;
    assert_int_equal(mod_change_logrotate_entry(NULL, &log2, NULL), 0);
    amxut_bus_handle_events();
    expected_removes += 2;       // Remove logrotate config file + remove crontab config
    expected_write_to_file += 2; // Add logrotate config file + crontab
    expected_system_calls += 2;  // Remove old crontabs + add 1 crontabs for new settings
    assert_calls();

    assert_int_equal(logrotate_list_size(), 4);

    amxc_var_clean(&log2);
    amxc_var_clean(&firewall);
    amxc_var_clean(&log);
    amxc_var_clean(&messages);
}

void test_add_logfiles(UNUSED void** state) {
    amxc_var_t messages;
    amxc_var_t* logfile1 = NULL;
    amxc_var_t* logfile2 = NULL;

    amxc_var_init(&messages);
    amxc_var_set_type(&messages, AMXC_VAR_ID_LIST);

    logfile1 = amxc_var_add(amxc_htable_t, &messages, NULL);
    amxc_var_add_key(cstring_t, logfile1, "action", "add");
    amxc_var_add_key(cstring_t, logfile1, "alias", "messages");
    amxc_var_add_key(cstring_t, logfile1, "directory", "/var/log");

    logfile2 = amxc_var_add(amxc_htable_t, &messages, NULL);
    amxc_var_add_key(cstring_t, logfile2, "action", "add");
    amxc_var_add_key(cstring_t, logfile2, "alias", "messages");
    amxc_var_add_key(cstring_t, logfile2, "directory", "/var/log");

    assert_int_equal(mod_logrotate_handle_script(NULL, &messages, NULL), 0);
    amxut_bus_handle_events();
    assert_calls(); // No calls done, only DM updates

    assert_int_equal(logrotate_list_size(), 4);

    amxc_var_clean(&messages);
}

void test_remove_logfiles(UNUSED void** state) {
    amxc_var_t messages;
    amxc_var_t* logfile1 = NULL;

    amxc_var_init(&messages);
    amxc_var_set_type(&messages, AMXC_VAR_ID_LIST);

    logfile1 = amxc_var_add(amxc_htable_t, &messages, NULL);
    amxc_var_add_key(cstring_t, logfile1, "action", "remove");
    amxc_var_add_key(cstring_t, logfile1, "alias", "messages");
    amxc_var_add_key(cstring_t, logfile1, "filename", "/var/log/messages.0");

    assert_int_equal(mod_logrotate_handle_script(NULL, &messages, NULL), 0);
    amxut_bus_handle_events();
    expected_removes += 1;       // Remove logfile /var/log/messages.0
    assert_calls();

    assert_int_equal(logrotate_list_size(), 4);

    amxc_var_clean(&messages);
}

void test_remove_entry(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t* alias_key = NULL;

    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    set_default_values(&data, "messages");
    alias_key = amxc_var_get_key(&data, "Alias", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(alias_key);
    amxc_var_delete(&alias_key);

    assert_int_equal(mod_delete_logrotate_entry(NULL, &data, NULL), -1);
    amxut_bus_handle_events();

    amxc_var_add_key(cstring_t, &data, "Alias", "messages");
    assert_int_equal(mod_delete_logrotate_entry(NULL, &data, NULL), 0);
    expected_removes += 2;       // Remove logrotate config + crontab config
    expected_write_to_file += 1; // Write crontab config file
    expected_system_calls += 2;  // Remove old crontab + add new crontab

    assert_calls();

    assert_int_equal(logrotate_list_size(), 3);

    amxc_var_clean(&data);
}

void test_minutes_to_crontab_string(UNUSED void** state) {
    char* result = minutes_to_crontab_string(15); // Every 15 minutes
    assert_string_equal(result, "*/15 * * * *");
    free(result);

    result = minutes_to_crontab_string(1); // Every minute
    assert_string_equal(result, "*/1 * * * *");
    free(result);

    result = minutes_to_crontab_string(60); // Every hour
    assert_string_equal(result, "0 */1 * * *");
    free(result);

    result = minutes_to_crontab_string(1 * 60 + 18); // Every hour and 18 minutes
    assert_string_equal(result, "18 */1 * * *");
    free(result);

    result = minutes_to_crontab_string(3 * 60 + 20); // Every 3 hours and 20 minutes
    assert_string_equal(result, "20 */3 * * *");
    free(result);

    result = minutes_to_crontab_string((23 * 60) + 59); // Every 23 hours and 59 minutes
    assert_string_equal(result, "59 */23 * * *");
    free(result);

    result = minutes_to_crontab_string((24 * 60) * 5 + (10 * 60) + 3); // Every 5 days, 10 hours and 3 minutes
    assert_null(result);
}

void test_convert_filesize(UNUSED void** state) {
    char size_order = '?';
    uint32_t size = 1;

    size_order = convert_filesize(&size);
    assert_int_equal(size, 1);
    assert_true(size_order == 'k');

    size = 999;
    size_order = convert_filesize(&size);
    assert_int_equal(size, 999);
    assert_true(size_order == 'k');

    size = 1200;
    size_order = convert_filesize(&size);
    assert_int_equal(size, 1);
    assert_true(size_order == 'M');

    size = 1000;
    size_order = convert_filesize(&size);
    assert_int_equal(size, 1);
    assert_true(size_order == 'M');

    size = 1000000;
    size_order = convert_filesize(&size);
    assert_int_equal(size, 1);
    assert_true(size_order == 'G');

    size = 1001000;
    size_order = convert_filesize(&size);
    assert_int_equal(size, 1.001);
    assert_true(size_order == 'G');
}
