/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <libgen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_dir.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logrotate_dm.h"

static int add_rotated_logfile(const char* name, void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char buffer[24];
    struct stat sb;
    struct tm* timeinfo;
    logrotate_entry_t* entry = (logrotate_entry_t*) priv;
    char* real_path = NULL;
    amxc_string_t timestamp;
    amxc_string_t filename_schemed;
    amxc_var_t* list_item = NULL;
    amxc_var_t updated_files;
    amxc_var_t ret;
    amxc_ts_t ts;

    amxc_var_init(&updated_files);
    amxc_var_init(&ret);
    amxc_string_init(&timestamp, 0);
    amxc_string_init(&filename_schemed, 0);

    when_str_empty(name, exit);
    when_null_trace(entry, exit, ERROR, "Could not convert entry");

    real_path = realpath(entry->name, NULL);
    when_null(real_path, exit);
    when_true(strcmp(name, real_path) == 0, exit);

    amxc_var_set_type(&updated_files, AMXC_VAR_ID_LIST);
    list_item = amxc_var_add_new(&updated_files);
    amxc_string_setf(&filename_schemed, "%s%s", URI_SCHEME_FILE, name);

    // Use stat to get file metadata like filesize and last modified time
    rv = stat(name, &sb);
    when_failed_trace(rv, exit, ERROR, "Could not stat %s", name);

    // Get last modified time of rotated logfile and parse it into ts
    timeinfo = gmtime(&sb.st_mtime);
    when_null(timeinfo, exit);
    strftime(buffer, 24, "%FT%TZ", timeinfo); // 2024-02-22T14:27:31Z
    when_failed_trace(amxc_ts_parse(&ts, buffer, strlen(buffer)), exit, ERROR, "Could not parse timestamp '%s'", buffer);

    amxc_var_set_type(list_item, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, list_item, "Action", "add");
    amxc_var_add_key(cstring_t, list_item, "ParentAlias", entry->alias);
    amxc_var_add_key(cstring_t, list_item, "Name", amxc_string_get(&filename_schemed, 0));
    amxc_var_add_key(uint32_t, list_item, "Size", sb.st_size);
    amxc_var_add_key(amxc_ts_t, list_item, "LastChange", &ts);

    rv = amxm_execute_function("self", MOD_CORE, "update-logrotate-logfile", &updated_files, &ret);
    when_failed_trace(rv, exit, WARNING, "Mod %s, func 'update-logrotate-logfile' returned %d", MOD_CORE, rv);

exit:
    free(real_path);
    amxc_var_clean(&ret);
    amxc_var_clean(&updated_files);
    amxc_string_clean(&filename_schemed);
    amxc_string_clean(&timestamp);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int add_rotated_files(logrotate_entry_t* entry, const char* directory) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t filter;
    char* file = NULL;

    amxc_string_init(&filter, 0);
    when_null(entry, exit);
    when_str_empty_status(entry->name, exit, rv = 0);

    file = strdup(entry->name);

    amxc_string_setf(&filter, "d_type == DT_REG && d_name matches '%s(\\..*)?$'", basename(file));
    rv = amxp_dir_scan(directory, amxc_string_get(&filter, 0), false, add_rotated_logfile, (void*) entry);

exit:
    amxc_string_clean(&filter);
    free(file);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int remove_rotated_file(const char* name, logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t filename_schemed;
    amxc_var_t* list_item = NULL;
    amxc_var_t updated_files;
    amxc_var_t ret;

    amxc_var_init(&updated_files);
    amxc_var_init(&ret);
    amxc_string_init(&filename_schemed, 0);

    when_str_empty(name, exit);
    when_null_trace(entry, exit, ERROR, "Could not convert entry");

    amxc_var_set_type(&updated_files, AMXC_VAR_ID_LIST);
    list_item = amxc_var_add_new(&updated_files);
    amxc_string_setf(&filename_schemed, "%s%s", URI_SCHEME_FILE, name);


    amxc_var_set_type(list_item, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, list_item, "Action", "remove");
    amxc_var_add_key(cstring_t, list_item, "ParentAlias", entry->alias);
    amxc_var_add_key(cstring_t, list_item, "Name", amxc_string_get(&filename_schemed, 0));

    rv = amxm_execute_function("self", MOD_CORE, "update-logrotate-logfile", &updated_files, &ret);
    when_failed_trace(rv, exit, WARNING, "Mod %s, func 'update-logrotate-logfile' returned %d", MOD_CORE, rv);

    rv |= remove(name);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&updated_files);
    amxc_string_clean(&filename_schemed);
    SAH_TRACEZ_OUT(ME);
    return rv;
}