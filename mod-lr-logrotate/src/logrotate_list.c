/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logrotate_list.h"
#include "logrotate_utils.h"

#define SAFE_DELETE(x) free(x); x = NULL;

static amxc_llist_t* logrotate_list = NULL;

size_t logrotate_list_size(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return amxc_llist_size(logrotate_list);
}

void logrotate_list_init(void) {
    SAH_TRACEZ_IN(ME);

    if(logrotate_list == NULL) {
        amxc_llist_new(&logrotate_list);
    }

    SAH_TRACEZ_OUT(ME);
}

void logrotate_list_clean(void) {
    SAH_TRACEZ_IN(ME);

    when_false_trace(logrotate_list_size() == 0, exit, ERROR, "Logrotate list should be emptied before cleaning it up.");

    amxc_llist_delete(&logrotate_list, NULL);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void logrotate_list_delete_item(amxc_llist_it_t* it) {
    SAH_TRACEZ_IN(ME);
    logrotate_entry_t* entry = amxc_container_of(it, logrotate_entry_t, it);
    when_null(it, exit);
    amxc_llist_it_take(it);

    SAFE_DELETE(entry->alias);
    SAFE_DELETE(entry->name);
    SAFE_DELETE(entry->user);
    SAFE_DELETE(entry->group);
    SAFE_DELETE(entry->config_file_path);
    SAFE_DELETE(entry);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

logrotate_entry_t* logrotate_list_find(const char* alias) {
    SAH_TRACEZ_IN(ME);
    logrotate_entry_t* ret_ptr = NULL;

    when_str_empty(alias, exit);
    amxc_llist_for_each(it, logrotate_list) {
        logrotate_entry_t* logrotate = amxc_container_of(it, logrotate_entry_t, it);
        if(strcmp(alias, logrotate->alias) == 0) {
            ret_ptr = logrotate;
            break;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return ret_ptr;
}

void logrotate_list_foreach(logrotate_action_cb_t cb) {
    SAH_TRACEZ_IN(ME);
    amxc_llist_for_each(it, logrotate_list) {
        logrotate_entry_t* entry = amxc_container_of(it, logrotate_entry_t, it);
        cb(entry);
    }
    SAH_TRACEZ_OUT(ME);
}

int logrotate_list_delete(const char* alias) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;

    when_str_empty(alias, exit);
    amxc_llist_for_each(it, logrotate_list) {
        logrotate_entry_t* logrotate = amxc_container_of(it, logrotate_entry_t, it);
        if(strcmp(alias, logrotate->alias) == 0) {
            logrotate_list_delete_item(it);
            retval = 0;
            break;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

logrotate_entry_t* logrotate_list_add(amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    logrotate_entry_t* entry = NULL;
    const char* alias = NULL;

    when_true_trace(amxc_var_is_null(args), exit, ERROR, "The given args parameter is invalid");
    alias = GET_CHAR(args, "Alias");
    when_null_trace(alias, exit, ERROR, "bad input alias value");

    when_false_trace(logrotate_list_find(alias) == NULL, exit, ERROR, "there is already a logrotate struct in the list");

    entry = (logrotate_entry_t*) calloc(1, sizeof(logrotate_entry_t));
    when_null_trace(entry, exit, ERROR, "Failed to execute calloc");

    entry->alias = strdup(alias);
    entry->config_file_path = NULL;
    entry->delay_compression = false;
    entry->copy_truncate = false;

    amxc_llist_append(logrotate_list, &(entry->it));

exit:
    SAH_TRACEZ_OUT(ME);
    return entry;
}