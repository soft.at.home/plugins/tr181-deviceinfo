/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <libgen.h>

#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb_be_mngr.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_logrotate.h"
#include "logrotate_list.h"
#include "logrotate_utils.h"
#include "logrotate_cron.h"
#include "logrotate_lr.h"
#include "logrotate_dm.h"

static amxc_var_t scripts_table;

const char* get_script(const char* name) {
    return GET_CHAR(&scripts_table, name);
}

bool allow_add_script(void) {
    return amxc_var_get_key(&scripts_table, "enable-add-script", AMXC_VAR_FLAG_DEFAULT) == NULL ? true : GET_BOOL(&scripts_table, "enable-add-script");
}

int mod_change_logrotate_interval(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    uint32_t new_interval = 0;

    when_null(params, exit);

    new_interval = GET_UINT32(params, NULL);
    rv = set_logrotate_interval(new_interval);
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_add_logrotate_entry(UNUSED const char* function_name,
                            amxc_var_t* params,
                            UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    logrotate_entry_t* entry = NULL;
    char* directory = NULL;

    when_null(params, exit);

    entry = logrotate_list_add(params);
    when_null(entry, exit);

    update_entry(entry, params, NULL);

    directory = strdup(entry->name);
    add_rotated_files(entry, dirname(directory));
    when_false(is_valid(entry), exit);

    rv = create_logrotate_config(entry);
    when_failed_trace(rv, exit, ERROR, "Failed to create logrotate config");
    rv = create_crontab_config();
    when_failed_trace(rv, exit, ERROR, "Failed to create crontab config");

exit:
    free(directory);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_change_logrotate_entry(UNUSED const char* function_name,
                               amxc_var_t* params,
                               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    logrotate_entry_t* entry = NULL;
    const char* alias = NULL;
    bool logrotate_changed = false;

    when_null(params, exit);

    alias = GET_CHAR(params, "Alias");
    entry = logrotate_list_find(alias);
    when_null(entry, exit);

    update_entry(entry, params, &logrotate_changed);
    when_false(is_valid(entry), exit);

    rv = 0;
    if(logrotate_changed) {
        rv |= delete_logrotate_config(entry);
        rv |= create_logrotate_config(entry);
    }
    when_failed_trace(rv, exit, ERROR, "Failed to update the logrotate config file");

    rv = create_crontab_config();
    when_failed_trace(rv, exit, ERROR, "Failed to create crontab config");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_delete_logrotate_entry(UNUSED const char* function_name,
                               amxc_var_t* params,
                               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* alias = NULL;
    logrotate_entry_t* entry = NULL;

    when_null(params, exit);
    alias = GET_CHAR(params, "Alias");
    entry = logrotate_list_find(alias);
    when_null(entry, exit);

    rv = remove_entry(entry);
    entry = NULL; // freed in remove_entry
    when_failed_trace(rv, exit, ERROR, "failed to remove entry");

    rv = create_crontab_config();
    when_failed_trace(rv, exit, ERROR, "Failed to create crontab config");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_logrotate_handle_script(UNUSED const char* function_name,
                                amxc_var_t* params,
                                UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null(params, exit);
    rv = 0;

    amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, params)) {
        amxc_var_t* file_data = amxc_var_from_llist_it(it);
        const char* alias = GET_CHAR(file_data, "alias");
        const char* action = GET_CHAR(file_data, "action");
        const char* directory = GET_CHAR(file_data, "directory");
        logrotate_entry_t* entry = logrotate_list_find(alias);

        when_null_trace(entry, exit, ERROR, "Got logrotate postrotate %s for invalid alias '%s'", action, alias);

        if(strcmp(action, "add") == 0) {
            rv |= add_rotated_files(entry, directory);
        } else if(strcmp(action, "remove") == 0) {
            rv |= remove_rotated_file(GET_CHAR(file_data, "filename"), entry);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_set_scripts(UNUSED const char* function_name,
                    amxc_var_t* params,
                    UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxc_var_copy(&scripts_table, params);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int rotate_logs(void) {
    SAH_TRACEZ_IN(ME);
    int rv = system(LOGROTATE_CMD_PATH " -f " LOGROTATE_CFG_PATH);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_CONSTRUCTOR lr_logrotate_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    SAH_TRACEZ_INFO(ME, "Starting mod-lr-logrotate");

    amxm_module_register(&mod, so, MOD_CTRL);
    amxm_module_add_function(mod, "change-lr-interval", mod_change_logrotate_interval);
    amxm_module_add_function(mod, "add-lr-entry", mod_add_logrotate_entry);
    amxm_module_add_function(mod, "change-lr-entry", mod_change_logrotate_entry);
    amxm_module_add_function(mod, "delete-lr-entry", mod_delete_logrotate_entry);
    amxm_module_add_function(mod, "handle-script", mod_logrotate_handle_script);
    amxm_module_add_function(mod, "set-scripts", mod_set_scripts);

    logrotate_list_init();
    amxc_var_init(&scripts_table);

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR lr_logrotate_stop(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_WARNING(ME, "Stopping mod-lr-logrotate");

    rotate_logs();

    logrotate_list_foreach(remove_entry);
    logrotate_list_clean();
    delete_crontab_config();
    amxc_var_clean(&scripts_table);

    SAH_TRACEZ_OUT(ME);
    return 0;
}