/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
// #include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_subproc.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logrotate_cron.h"
#include "logrotate_list.h"
#include "logrotate_utils.h"

#define GLOBAL_CRONTAB_FILE "/etc/crontabs/root"
#define INTERMEDIATE_CRONTAB_FILE "/etc/crontabs/logrotate"

static amxc_string_t crontab;
static bool crontab_valid = false;
static bool crontab_initialized = false;
static bool crond_launched = false;
static uint32_t crontab_interval = 0;

#define WRITE_CRONTAB_PART(function, string_ref, value) { \
        if((value) == 0) { \
            function(string_ref, "* "); \
        } else { \
            function(string_ref, "*/%d ", (value)); \
        } \
}

char* minutes_to_crontab_string(int minutes) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t buffer;
    char* result = NULL;
    int hours = minutes / 60;
    int mins = minutes % 60;

    amxc_string_init(&buffer, 0);
    when_true(minutes > (23 * 60 + 59), exit);

    if(hours > 0) {
        amxc_string_setf(&buffer, "%d ", mins);
    } else {
        WRITE_CRONTAB_PART(amxc_string_setf, &buffer, mins);
    }
    WRITE_CRONTAB_PART(amxc_string_appendf, &buffer, hours);
    amxc_string_appendf(&buffer, "* * *");

    result = amxc_string_take_buffer(&buffer);

exit:
    amxc_string_clean(&buffer);
    SAH_TRACEZ_OUT(ME);
    return result;
}

int set_logrotate_interval(uint32_t interval) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_true_status(interval == crontab_interval, exit, rv = 0);

    crontab_interval = interval;
    invalidate_crontab_config();
    rv = create_crontab_config();

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void invalidate_crontab_config(void) {
    SAH_TRACEZ_IN(ME);
    crontab_valid = false;
    SAH_TRACEZ_OUT(ME);
}

static void launch_crond(void) {
    SAH_TRACEZ_IN(ME);
    int pid = 0;
    FILE* file = NULL;
    int error_process = 0;
    amxp_subproc_t* process_crond = NULL;
    const char* cmd[] = { "/etc/init.d/cron", "start", NULL };

    file = fopen("/var/run/crond.pid", "r");
    if(file != NULL) {
        if((fscanf(file, "%d", &pid) == 1) && (pid >= 1)) {
            SAH_TRACEZ_INFO(ME, "Crond is already running (PID: %d)", pid);
        } else {
            SAH_TRACEZ_WARNING(ME, "Cannot read /var/run/crond.pid");
        }
        fclose(file);
    } else {
        when_failed_trace(amxp_subproc_new(&process_crond), exit, ERROR, "Failed to launch crond");
        error_process = amxp_subproc_vstart(process_crond, (char**) cmd);
        if(error_process) {
            SAH_TRACEZ_ERROR(ME, "Failed to run crond with error %d", error_process);
        } else {
            SAH_TRACEZ_INFO(ME, "Crond launched");
            crond_launched = true;
        }
        amxp_subproc_delete(&process_crond);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

int create_crontab_config(void) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t croncommand;
    char* interval_time = NULL;
    int rv = -1;

    amxc_string_init(&croncommand, 0);

    when_true_status(crontab_valid && crontab_initialized, exit, rv = 0);

    rv = delete_crontab_config();
    when_failed_trace(rv, exit, ERROR, "Failed to delete old crontab config");
    amxc_string_init(&crontab, 0);

    interval_time = minutes_to_crontab_string(crontab_interval);

    amxc_string_setf(&crontab, "#############################  START of logrotate crontabs #############################\n");
    amxc_string_appendf(&crontab, "# tr181-deviceinfo logrotate generated crontabs.\n");
    amxc_string_appendf(&crontab, "#    do not change any values in this logrotate crontab block.\n");
    amxc_string_appendf(&crontab, "%s %s %s\n", interval_time, LOGROTATE_CMD_PATH, LOGROTATE_CFG_PATH);
    logrotate_list_foreach(append_entry_to_crontab_config);
    amxc_string_appendf(&crontab, "##############################  END of logrotate crontabs ##############################\n");

    rv = write_to_file(INTERMEDIATE_CRONTAB_FILE, amxc_string_get(&crontab, 0));
    when_failed_trace(rv, exit, ERROR, "Failed to write crontab config to " INTERMEDIATE_CRONTAB_FILE);

    amxc_string_setf(&croncommand, "(%scat " INTERMEDIATE_CRONTAB_FILE ") | crontab -", file_exists(GLOBAL_CRONTAB_FILE) ? "crontab -l && " : "");
    rv = system(amxc_string_get(&croncommand, 0));
    when_failed_trace(rv, exit, ERROR, "Failed to apply crontab config from " INTERMEDIATE_CRONTAB_FILE);

    crontab_valid = true;
    crontab_initialized = true;
    if(!crond_launched) {
        launch_crond();
    }

exit:
    amxc_string_clean(&croncommand);
    free(interval_time);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int delete_crontab_config(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    amxc_string_clean(&crontab);

    when_false_status(crontab_initialized, exit, rv = 0);

    if(file_exists(GLOBAL_CRONTAB_FILE)) {
        rv = system("(crontab -l | grep -vxFf " INTERMEDIATE_CRONTAB_FILE ") | crontab -");
        when_failed_trace(rv, exit, ERROR, "Failed to remove entries in " INTERMEDIATE_CRONTAB_FILE " from crontab config");
    }

    rv = remove(INTERMEDIATE_CRONTAB_FILE);
    when_failed_trace(rv, exit, ERROR, "Failed to remove crontab config file " INTERMEDIATE_CRONTAB_FILE);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void add_crontab_roll_over_config(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    char* force_logrotate_time = NULL;

    when_null(entry, exit);
    when_true(entry->roll_over == 0, exit);
    when_str_empty(entry->config_file_path, exit);

    force_logrotate_time = minutes_to_crontab_string(entry->roll_over);
    amxc_string_appendf(&crontab, "%s %s -f %s\n", force_logrotate_time, LOGROTATE_CMD_PATH, entry->config_file_path);

exit:
    free(force_logrotate_time);
    SAH_TRACEZ_OUT(ME);
}

static void add_crontab_retention_config(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);

    when_null(entry, exit);
    when_true(entry->retention == 0, exit);
    when_str_empty(entry->name, exit);

    amxc_string_appendf(&crontab, "*/1 * * * * %s %s %s %d\n", get_script(LR_REMOVE_SCRIPT), entry->name, entry->alias, entry->retention); // Check every minute for files older than retention minutes

exit:
    SAH_TRACEZ_OUT(ME);
}

int append_entry_to_crontab_config(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    add_crontab_roll_over_config(entry);
    add_crontab_retention_config(entry);
    SAH_TRACEZ_OUT(ME);
    return 0;
}
