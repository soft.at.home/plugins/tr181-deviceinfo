/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "logrotate_utils.h"
#include "logrotate_list.h"
#include "logrotate_cron.h"
#include "logrotate_lr.h"

const char* compression_type_to_string(compression_type_t compression) {
    SAH_TRACEZ_IN(ME);
    static const char* TYPE[] = {"None", "GZIP", "BZIP2", "LZO", "XZ", "ZSTD"};

    SAH_TRACEZ_OUT(ME);
    return (compression >= 0 && compression < LAST) ? TYPE[compression] : NULL;
}

compression_type_t compression_type_from_string(const char* compression) {
    SAH_TRACEZ_IN(ME);
    compression_type_t c = NONE;

    when_str_empty(compression, exit);

    for(; c < LAST; c++) {
        const char* result = compression_type_to_string(c);
        if(strcmp(compression, result) == 0) {
            break;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return c;
}

/* Converts (if possible) the size in Kb to Mb or Gb.
 * The size parameter is changed in this function
 * Returns: a character representing the size: k -> Kb, M -> Mb, G -> Gb
 */
char convert_filesize(uint32_t* size) {
    char size_order = 'k';

    if(*size >= 1000000) {
        size_order = 'G';
        *size /= 1000000;
    } else if(*size >= 1000) {
        size_order = 'M';
        *size /= 1000;
    }
    return size_order;
}

bool is_valid(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    bool valid = false;

    when_null(entry, exit);
    when_str_empty_trace(entry->name, exit, ERROR, "Name should not be empty");
    when_true(entry->compression >= LAST, exit);

    valid = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return valid;
}

void update_entry(logrotate_entry_t* entry, amxc_var_t* args, bool* update_logrotate_config) {
    SAH_TRACEZ_IN(ME);
    const char* compression = NULL;
    bool logrotate_config_changed = false;

    when_null(entry, exit);
    when_null(args, exit);

    if(amxc_var_get_key(args, "Name", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        free(entry->name);
        entry->name = STR_EMPTY(GET_CHAR(args, "Name")) ? strdup("") : strdup(GET_CHAR(args, "Name"));
        invalidate_crontab_config();
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "NumberOfFiles", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        entry->number_of_files = GET_UINT32(args, "NumberOfFiles");
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "MaxFileSize", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        entry->max_file_size = GET_UINT32(args, "MaxFileSize");
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "RollOver", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        entry->roll_over = GET_UINT32(args, "RollOver");
        invalidate_crontab_config();
    }
    if(amxc_var_get_key(args, "Retention", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        entry->retention = GET_UINT32(args, "Retention");
        invalidate_crontab_config();
    }
    if(amxc_var_get_key(args, "Compression", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        compression = GET_CHAR(args, "Compression");
        when_str_empty(compression, exit);
        entry->compression = compression_type_from_string(compression);
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "DelayCompression", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        entry->delay_compression = GET_BOOL(args, "DelayCompression");
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "CopyTruncate", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        entry->copy_truncate = GET_BOOL(args, "CopyTruncate");
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "SuUser", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        free(entry->user);
        entry->user = STR_EMPTY(GET_CHAR(args, "SuUser")) ? NULL : strdup(GET_CHAR(args, "SuUser"));
        logrotate_config_changed = true;
    }
    if(amxc_var_get_key(args, "SuGroup", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        free(entry->group);
        entry->group = STR_EMPTY(GET_CHAR(args, "SuGroup")) ? NULL : strdup(GET_CHAR(args, "SuGroup"));
        logrotate_config_changed = true;
    }

    if(update_logrotate_config != NULL) {
        *update_logrotate_config = logrotate_config_changed;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int remove_entry(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null(entry, exit);

    rv = delete_logrotate_config(entry);
    rv |= logrotate_list_delete(entry->alias);
    entry = NULL; // freed in logrotate_list_delete
    invalidate_crontab_config();

exit:
    return rv;
    SAH_TRACEZ_OUT(ME);
}

bool file_exists(const char* filename) {
    return access(filename, F_OK) == 0;
}

#ifndef UNIT_TEST
int write_to_file(const char* filename, const char* data) {
    SAH_TRACEZ_IN(ME);
    FILE* fp = NULL;

    fp = fopen(filename, "w");
    when_null_trace(fp, exit, ERROR, "Could not open %s for writing", filename);
    fprintf(fp, "%s", data);
    fclose(fp);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}
#endif