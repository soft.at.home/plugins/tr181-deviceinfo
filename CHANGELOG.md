# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.35.5 - 2024-12-22(09:15:05 +0000)

### Other

- [SSH] Make getDebug working with non-root ssh user.

## Release v2.35.4 - 2024-12-03(10:59:54 +0000)

### Other

- - Logs tar.gz are using date iso index

## Release v2.35.3 - 2024-11-21(12:56:52 +0000)

### Other

- [tr181-pcm] Backup File Copy Timing Issue in WebUI with Encryption Enabled

## Release v2.35.2 - 2024-10-22(14:14:24 +0000)

### Other

- - Add logrotate append script

## Release v2.35.1 - 2024-10-22(13:42:23 +0000)

### Other

- - tmpfs_download_mount.sh : usr/bin: unknown operand
- -[Upgrade] The upgrade test did not pass on the WebUI for WNC-CHR2
- changed destination: "/etc/environment-deviceinfo" to destination: "/etc/environment"

## Release v2.35.0 - 2024-10-18(12:44:22 +0000)

## Release v2.34.0 - 2024-10-17(09:48:04 +0000)

### Other

- - meta amx net core blocked by tr181-deviceinfo

## Release v2.33.53 - 2024-10-14(08:04:51 +0000)

### Other

- - tmpfs_download_mount.sh : usr/bin: unknown operand

## Release v2.33.52 - 2024-10-10(18:09:26 +0000)

### Other

- - Add persistent logs to getDebug

## Release v2.33.51 - 2024-10-07(06:55:25 +0000)

## Release v2.33.49 - 2024-10-02(18:44:21 +0000)

### Other

- - DeviceInfo.X_PRPL-COM_LastUpgradeDate has incorrect date

## Release v2.33.48 - 2024-10-01(09:39:22 +0000)

### Other

- - [odl] parameters being set twice on DeviceInfo.

## Release v2.33.47 - 2024-09-26(14:29:26 +0000)

## Release v2.33.46 - 2024-09-13(11:51:25 +0000)

### Other

- - Implement logrotate for logfiles

## Release v2.33.45 - 2024-09-11(16:04:43 +0000)

### Other

- - DeviceInfo.X_VZ-COM_LastSoftwareUpdateStamp and DeviceInfo.X_PRPL-COM_LastUpgradeDate has incorrect date

## Release v2.33.44 - 2024-09-10(07:07:54 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v2.33.43 - 2024-09-05(11:20:27 +0000)

### Fixes

- - [swupdate] More flexible version number format

## Release v2.33.42 - 2024-09-02(15:55:58 +0000)

### Other

- - Firmware upgrade fails when /tmp is full

## Release v2.33.41 - 2024-08-21(21:57:25 +0000)

### Other

- - It should be possible to tar rotated messages

## Release v2.33.40 - 2024-08-16(09:56:50 +0000)

### Other

- - Log rotate is skipping number

## Release v2.33.39 - 2024-08-06(09:48:12 +0000)

### Fixes

- [tr181-DeviceInfo] Wrong FriendlyName of the repeater

## Release v2.33.38 - 2024-08-06(08:33:19 +0000)

### Fixes

- [DeviceInfo][Process] Process list is not updated

## Release v2.33.37 - 2024-08-06(08:15:17 +0000)

### Other

- - Firmware upgrade fails when /tmp is full

## Release v2.33.36 - 2024-08-03(05:39:24 +0000)

### Other

- [Init] Runtime dependency on 'Users.User' only required on PRPL OS

## Release v2.33.35 - 2024-08-02(14:43:34 +0000)

### Other

- [Init] Runtime dependency on 'Users.User' only required on PRPL OS

## Release v2.33.34 - 2024-07-31(08:11:21 +0000)

### Fixes

- [tr181-deviceinfo] Upload RPC does not use multipart body

## Release v2.33.33 - 2024-07-17(09:36:09 +0000)

### Fixes

- Better shutdown script

## Release v2.33.32 - 2024-07-16(08:01:29 +0000)

### Other

- User configuration lost after reboot

## Release v2.33.31 - 2024-07-11(11:13:16 +0000)

### Other

- - Rotate logs when plugin stops

## Release v2.33.30 - 2024-07-11(10:51:02 +0000)

### Other

- Add timestamp of last successful firmware upgrade

## Release v2.33.29 - 2024-07-11(10:20:25 +0000)

### Other

- - [mod-fwi-swupdate] Only UBI is supported  to get rootfs versions

## Release v2.33.28 - 2024-07-11(05:57:04 +0000)

### Other

- [Logrotate] Sometimes the first rotation is not done

## Release v2.33.27 - 2024-07-08(06:58:03 +0000)

## Release v2.33.26 - 2024-06-28(10:15:51 +0000)

### Other

- - [mod-fwi-swupdate] Only UBI is supported  to get rootfs versions

## Release v2.33.25 - 2024-06-25(11:00:28 +0000)

### Fixes

- scripts: 00_environment_deviceinfo.sh: fix script syntax

### Other

- can not retrieve software version

## Release v2.33.24 - 2024-06-25(08:52:41 +0000)

### Other

- [DeviceInfo] VendorConfigFile instance must be created when we call the PCM.Backup

## Release v2.33.23 - 2024-06-21(09:45:42 +0000)

### Other

- - [Logrotate] Add the possibility to compress from the first time

## Release v2.33.22 - 2024-06-20(15:51:16 +0000)

### Other

- HostName in SystemSettings should not end with special characters as dash and underscore

## Release v2.33.21 - 2024-06-17(15:26:34 +0000)

### Other

- TR181-DeviceInfo have empty FirmwareImage instance

## Release v2.33.20 - 2024-06-14(12:24:28 +0000)

### Other

- - [DeviceConfig] Add ability to rollback firmware (data-model part)

## Release v2.33.19 - 2024-06-13(18:49:10 +0000)

### Other

- - [tr181-deviceinfo] make CACertificate reboot persistent

## Release v2.33.18 - 2024-06-13(11:49:57 +0000)

## Release v2.33.17 - 2024-06-12(15:08:07 +0000)

### Other

- - [DeviceConfig] Add ability to rollback firmware

## Release v2.33.16 - 2024-06-10(10:46:35 +0000)

## Release v2.33.15 - 2024-06-07(11:42:43 +0000)

### Other

- [TR181] CA location configurable via Datamodel

## Release v2.33.14 - 2024-06-03(07:53:35 +0000)

### Other

- - [tr181-deviceinfo] Clash between rpc and parameter name

## Release v2.33.13 - 2024-05-30(07:15:09 +0000)

### Other

- [mxl-osp][full-featured-prplwrt] ModelName is empty for mxl-osp build

## Release v2.33.12 - 2024-05-29(15:11:08 +0000)

### Fixes

- [DeviceInfo] getIntances fails on Device.DeviceInfo.ProcessStatus.Process.

## Release v2.33.11 - 2024-05-13(09:17:28 +0000)

### Fixes

- Improve hardware MAC address detection on boot

## Release v2.33.10 - 2024-05-09(07:35:16 +0000)

### Fixes

- Issue HOP-6398 [TR181-DeviceInfo] Hostname should be available sooner

## Release v2.33.9 - 2024-04-25(13:27:11 +0000)

## Release v2.33.8 - 2024-04-18(10:34:59 +0000)

### Fixes

- set hardware and software version after reboot

## Release v2.33.7 - 2024-04-18(09:01:59 +0000)

### Fixes

- [tr181-DeviceInfo] The bus access method should be AMXB_PROTECTED

## Release v2.33.6 - 2024-04-17(14:47:47 +0000)

### Fixes

- [wnc-chr2] swupdate is not working

## Release v2.33.5 - 2024-04-15(12:54:37 +0000)

### Other

- get software version from runtime environment

## Release v2.33.4 - 2024-04-11(09:11:51 +0000)

### Fixes

- [DeviceInfo] properly rework differentiation between AP and GW

## Release v2.33.3 - 2024-04-10(10:10:15 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.33.2 - 2024-03-29(14:59:21 +0000)

### Fixes

- [tr181-pcm] Saved and defaults odl should not be included in the backup files

## Release v2.33.1 - 2024-03-28(10:13:59 +0000)

### Fixes

- [tr181-deviceinfo] VendorLogFile and LogRotate instances are not created when there is a Syslog.Action.Logfile with empty path

## Release v2.33.0 - 2024-03-23(13:17:43 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.32.1 - 2024-03-22(14:18:47 +0000)

### Changes

- [tr181-deviceinfo] Generation of VendorLogFile should be configurable

## Release v2.32.0 - 2024-03-22(13:49:11 +0000)

### New

- [DeviceInfo.VendorLogFile] DebugInfo file must always have index 1 and always update the same instance

## Release v2.31.0 - 2024-03-21(09:58:01 +0000)

### New

-  [tr181-deviceinfo] Add support for running scripts during logrotate process

## Release v2.30.0 - 2024-03-15(16:18:26 +0000)

### New

- [TR181-DeviceInfo]File upload enhancements

## Release v2.29.1 - 2024-03-11(13:51:14 +0000)

### Fixes

-  [tr181-syslog] When hostname changes syslog needs to be restarted

## Release v2.29.0 - 2024-03-11(13:12:57 +0000)

### New

-  [tr181-syslog][logrotate] LogFile entries defined in Syslog.Action.Logfile should be logrotated

## Release v2.28.1 - 2024-03-11(12:10:49 +0000)

### Fixes

- [TR181-Syslog] log syslog to a dedicated destination

## Release v2.28.0 - 2024-03-07(13:55:49 +0000)

### New

- [TR181-Syslog] Implement VendorLogFileRef

## Release v2.27.2 - 2024-03-01(16:12:06 +0000)

### Fixes

-  [tr181-deviceinfo][logrotate] Crontabs are not created when crontab file does not exist

## Release v2.27.1 - 2024-02-29(13:29:28 +0000)

### Fixes

- [tr181-DeviceInfo] pcm-manger API calls must be updated

## Release v2.27.0 - 2024-02-27(15:52:20 +0000)

### New

-  [tr181-syslog][log rotate] Add support for log rotate for log files

## Release v2.26.0 - 2024-02-20(10:28:18 +0000)

### New

- DUT doesn't resolve its hostname

## Release v2.25.1 - 2024-02-19(09:15:03 +0000)

### Changes

- use libamxut for unit tests

## Release v2.25.0 - 2024-02-13(11:04:43 +0000)

### New

- Changing Device.DeviceInfo.HostName is not reflecting immediately. Requires a reboot

## Release v2.24.1 - 2024-02-08(17:05:03 +0000)

### Fixes

- [DeviceInfo][PCM] Call the Backup/Restore methods asynchronously

## Release v2.24.0 - 2024-01-31(15:38:07 +0000)

### New

- [DHCPv6C][MAC Address Cloning] If the MAC address changes, DHCPv6 client must be retriggered

## Release v2.23.2 - 2024-01-26(10:16:43 +0000)

### Other

- [OJO_PRPL][TR69] DeviceInfo.ProvisioningCode is not upgrade persistent

## Release v2.23.1 - 2024-01-25(13:33:11 +0000)

### Other

- [tr181-deviceinfo] ModelName is empty

## Release v2.23.0 - 2024-01-22(12:36:18 +0000)

### New

- - [MAC Address Cloning] API to Restore the factory MAC address

## Release v2.22.0 - 2024-01-16(11:29:40 +0000)

### New

- - [DHCPv6C][MAC Address Cloning] If the MAC address changes, DHCPv6 client must be retriggered

## Release v2.21.6 - 2024-01-10(12:08:43 +0000)

### Other

- Cleanup duplicate script to calculate macaddresses

## Release v2.21.5 - 2024-01-04(12:34:49 +0000)

### Other

- [qcom-ipq8074] ethernet KO after firstboot on tmcssp config

## Release v2.21.4 - 2023-12-19(21:19:35 +0000)

### Fixes

- [ProcessMonitor] [DeviceInfo] cleanup plugin

## Release v2.21.3 - 2023-12-12(15:45:02 +0000)

### Fixes

- [deviceinfo-manager]  Not correctly differentation between AP and GW configuration

## Release v2.21.2 - 2023-12-07(15:31:52 +0000)

### Other

- Handle removal of the overlay during first boot

## Release v2.21.1 - 2023-12-06(08:36:22 +0000)

### Changes

- Handle removal of the overlay during first boot

## Release v2.21.0 - 2023-12-05(11:25:51 +0000)

### New

- [DeviceInfo][VendorLogFile][sFTP] It must be possible to upload vendor log files using (S)FTP.

## Release v2.20.10 - 2023-12-01(18:18:12 +0000)

### Other

- Move environment parameters to definition

## Release v2.20.9 - 2023-11-28(20:34:42 +0000)

### Fixes

- [umdns][TR181-DNSSD] - remove "lan" prefix from service announced by umdns

### Other

- [DeviceInfo] Some parameters should always load from environment variables

## Release v2.20.8 - 2023-11-16(08:10:32 +0000)

### Fixes

- [TR181-DeviceInfo] make the dbready scripts compatible for non sbl boards & enable it by default

## Release v2.20.7 - 2023-11-15(16:15:16 +0000)

### Fixes

- [DeviceInfo] Fix amxm module loading and usage

## Release v2.20.6 - 2023-11-13(14:57:42 +0000)

## Release v2.20.5 - 2023-11-10(15:21:22 +0000)

### Fixes

- [TR181-DeviceInfo][MAC Address] rename the BaseMAC parameter to BaseMACAddress in the ap/hgw defaults

## Release v2.20.4 - 2023-11-10(14:14:32 +0000)

### Other

- [DATAMODEL] Incoherences under Device.DeviceInfo.FirmwareImage.1

## Release v2.20.3 - 2023-11-07(14:42:45 +0000)

### Fixes

- [mod-fwi-swupdate] WebUI upgrade failing: Rescue scenario wrongly selected instead of nominal scenario

## Release v2.20.2 - 2023-11-07(14:18:49 +0000)

### Fixes

- [mod-fwi-swupdate] WebUI upgrade failing: Rescue scenario wrongly selected instead of nominal scenario

## Release v2.20.1 - 2023-11-07(10:49:37 +0000)

### Other

- Allow to dump containers logs

## Release v2.20.0 - 2023-11-07(10:10:55 +0000)

### New

- [AP Config][HostName] The default HostName of a repeater must be prplAP-<MAC>

## Release v2.19.0 - 2023-11-06(09:49:42 +0000)

### New

- [TR181-pcm] Add support for different backup file types

## Release v2.18.4 - 2023-11-03(08:18:25 +0000)

### Fixes

- [tr181-deviceinfo]Upgrade fails with permission error

## Release v2.18.3 - 2023-10-28(08:14:56 +0000)

### Other

- - [ACL][webui] Fix missing ACL for webui test / delete webui.json

## Release v2.18.2 - 2023-10-28(07:09:57 +0000)

### Changes

- CLONE - Include extensions directory in ODL

## Release v2.18.1 - 2023-10-25(13:36:25 +0000)

### Changes

- Check at runtime if 00_environment_mfg_helper should run

## Release v2.18.0 - 2023-10-23(13:53:36 +0000)

### New

- [TR181-DeviceInfo][MAC Address] rename the BaseMAC parameter to BaseMACAddress

## Release v2.17.0 - 2023-10-20(09:51:35 +0000)

### New

- Add fallback script for calculating extra mac addresses

## Release v2.16.2 - 2023-10-19(14:51:39 +0000)

### Other

- - [DeviceInfo] Create customer compliant DeviceInfo Module

## Release v2.16.1 - 2023-10-19(13:12:07 +0000)

### Other

- [deviceinfo-manager][prplOS] console banner should contain version information

## Release v2.16.0 - 2023-10-13(15:16:19 +0000)

### New

- [amxrt][no-root-user][capability drop]deviceinfo-manager must be adapted to run as non-root and lmited capabilities

## Release v2.15.2 - 2023-10-13(14:12:21 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v2.15.1 - 2023-10-13(11:18:47 +0000)

### Fixes

- - [DeviceInfo] Controllers are not persistent

## Release v2.15.0 - 2023-10-10(11:34:37 +0000)

### New

- [swupdate][mod_fwi-swupdate][libfiletransfer] Mutual authenticity and HTTPS support for Download() function

## Release v2.14.0 - 2023-10-09(12:11:24 +0000)

### New

- [DeviceInfo] Environment script cleanup and allow for modular environment definition

## Release v2.13.5 - 2023-09-28(09:36:05 +0000)

### Other

- - [ACL][webui] Fix missing ACL for webui test

## Release v2.13.4 - 2023-09-22(12:22:50 +0000)

### Fixes

- [DeviceInfo] Remove the prefix of the pcm-manager object

## Release v2.13.3 - 2023-09-15(14:29:34 +0000)

### Fixes

- DeviceInfo is blocking for a long time on boot

## Release v2.13.2 - 2023-08-31(09:30:20 +0000)

### Fixes

- [tr181-DeviceInfo] Crash of ReadUsersetting and WriteUsersetting rpcs

## Release v2.13.1 - 2023-08-31(09:13:14 +0000)

### Fixes

- [v0.8.0, v0.8.1 and v0.8.2] - DeviceInfo.SoftwareVersion is not set the real version that is flashed

## Release v2.13.0 - 2023-08-24(11:19:55 +0000)

### New

- Trigger restore on upgrade

## Release v2.12.1 - 2023-08-24(10:16:33 +0000)

### Fixes

- [DeviceInfo] VendorConfigFile.UserSettings.{i}.Name parameter is not reboot persistent

## Release v2.12.0 - 2023-08-18(07:43:43 +0000)

### New

- [DeviceInfo] Version information should be extracted from FIT header when secure bootloader is used

## Release v2.11.10 - 2023-08-02(13:01:41 +0000)

### Other

- [SWUpdate][mod_fwi-swupdate] module should reboot the board on successful activation

## Release v2.11.9 - 2023-07-31(08:22:16 +0000)

### Fixes

- Some parameters are not restored after reboot

## Release v2.11.8 - 2023-07-31(07:54:08 +0000)

### Fixes

- [Upgrade][DeviceInfo] wrong upgrade_key path

## Release v2.11.7 - 2023-07-26(14:51:58 +0000)

### Other

- [SWUpdate][mod_fwi-swupdate] Create hw-revision and sw-version file on module startup

## Release v2.11.6 - 2023-07-26(12:22:05 +0000)

### Other

- TR181_DeviceInfo.FirmwareImage object is not filled correctly

## Release v2.11.5 - 2023-07-12(12:24:24 +0000)

### Fixes

- [PRPL][SAFRAN][ACS] Fault code when launching a setParameterValue script using the ACS

## Release v2.11.4 - 2023-07-10(13:50:54 +0000)

### Changes

- [deviceinfo-manager] Implement defaults.d directory

## Release v2.11.3 - 2023-07-10(11:19:19 +0000)

### Fixes

- [DeviceInfo]Replace the path of the VendorConfigFile.UserSettings with the correct one

## Release v2.11.2 - 2023-07-05(07:23:05 +0000)

### Fixes

- [ubus-cli] SearchPath syntax on Device.DeviceInfo.ProcessStatus.Process.{i}. is returning no data

## Release v2.11.1 - 2023-07-05(07:09:21 +0000)

### Fixes

- Init script has no shutdown function

## Release v2.11.0 - 2023-06-22(06:55:41 +0000)

### New

- add X_PRPL-COM_Country to deviceInfo

## Release v2.10.2 - 2023-06-10(07:29:03 +0000)

### Fixes

- Crash during shutdown

## Release v2.10.1 - 2023-06-07(08:37:25 +0000)

### Fixes

- [TR181-Deviceinfo] Suspicious logs at startup

## Release v2.10.0 - 2023-06-05(15:10:42 +0000)

### New

- [SMD] Link DeviceInfo parameters to Manufacturing Data

## Release v2.9.0 - 2023-06-05(11:20:38 +0000)

### New

- [TR181-DeviceInfo][FCGI] Add rpc to read/write a file

## Release v2.8.1 - 2023-06-01(12:56:56 +0000)

### Other

- [KPN][USP] The box doesn't send TransferComplete! event after upgrade

## Release v2.8.0 - 2023-06-01(10:15:23 +0000)

### New

- add ACLs permissions for cwmp user

## Release v2.7.5 - 2023-05-25(09:03:26 +0000)

### Other

- [DeviceInfo][Glinet] TR69 test is failing due to empty DeviceInfo.ManufacturerOUI

## Release v2.7.4 - 2023-05-24(06:58:01 +0000)

### Other

- - [HTTPManager][WebUI] Create plugin's ACLs permissions

## Release v2.7.3 - 2023-05-23(11:02:35 +0000)

### Fixes

- [tr181-deviceinfo] Export the ULIMIT_CONFIGURATION env variable

## Release v2.7.2 - 2023-05-04(10:02:20 +0000)

### Fixes

- optimize /etc/environment script

## Release v2.7.1 - 2023-04-17(11:26:49 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v2.7.0 - 2023-03-15(10:47:50 +0000)

### New

- [tr181-deviceinfo] Use POST method instead of PUT

## Release v2.6.2 - 2023-03-09(12:15:56 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v2.6.1 - 2023-03-02(09:05:37 +0000)

### Other

- [prpl][SWUpdate][DeviceInfo] Use storage swu file on Download function

## Release v2.6.0 - 2023-02-21(15:56:55 +0000)

### Other

- [PRPL] Make MSS work on prpl config

## Release v2.5.2 - 2023-02-21(10:58:57 +0000)

### Other

- Fix odl documentation generation

## Release v2.5.1 - 2023-02-13(15:37:55 +0000)

## Release v2.5.0 - 2023-01-09(11:52:52 +0000)

### New

- [TR181-DeviceInfo] HardwareVersion, SoftwareVersion

## Release v2.4.1 - 2022-12-09(08:27:04 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v2.4.0 - 2022-11-17(12:10:40 +0000)

### Other

- - [libfiletransfer] add reply handler to request new API arguments

## Release v2.3.0 - 2022-11-08(09:11:20 +0000)

### New

- [tr181-DeviceInfo] Support the DeviceInfo.VendorConfigFile. datamodel

## Release v2.2.1 - 2022-10-25(13:34:06 +0000)

### Other

- [DeviceInfo] Support the DeviceInfo.VendorLogFile. datamodel
- Issue: HOP-1511[DeviceInfo] Support the DeviceInfo.VendorLogFile. datamodel

## Release v2.2.0 - 2022-10-24(15:49:10 +0000)

### New

- Issue: HOP-1511[DeviceInfo] Support the DeviceInfo.VendorLogFile. datamodel

## Release v2.1.2 - 2022-10-13(08:20:52 +0000)

### Other

- Improve plugin boot order

## Release v2.1.1 - 2022-10-10(06:50:18 +0000)

### Other

- Implement swupdate on wnc board

## Release v2.1.0 - 2022-09-27(12:29:19 +0000)

### New

- Add BASEMACADDRESS to /etc/environment

## Release v2.0.0 - 2022-09-27(11:51:28 +0000)

### Other

- Implement swupdate on wnc board

## Release v1.2.21 - 2022-09-21(07:14:16 +0000)

### Other

- [USP][CDROUTER] The upgrade firmware tests are failing

## Release v1.2.20 - 2022-09-09(13:06:57 +0000)

### Other

- DeviceInfo add support for certification authentication for https downloads

## Release v1.2.19 - 2022-09-08(15:29:36 +0000)

### Other

- DeviceInfo add support for certification authentication for https downloads

## Release v1.2.18 - 2022-08-01(07:38:43 +0000)

### Fixes

- [DeviceInfo][NEC WX3000HP] TR69 and LCM failing due to empty DeviceInfo.SerialNumber

## Release v1.2.17 - 2022-07-28(10:15:04 +0000)

### Fixes

- sysinit done optional

## Release v1.2.16 - 2022-07-26(07:19:34 +0000)

### Other

- sysinit done optiona

## Release v1.2.15 - 2022-07-12(11:36:05 +0000)

### Fixes

- [tr181-DeviceInfo][async] The Download (and Activate) function in DeviceInfo must be marked as async

## Release v1.2.14 - 2022-07-11(11:21:45 +0000)

### Other

- Need to scrub components.h on open source

## Release v1.2.13 - 2022-07-06(13:44:54 +0000)

### Changes

- Turn off import-dbg

## Release v1.2.12 - 2022-07-05(07:31:27 +0000)

### Other

- [DeviceInfo] [FirmwareImage] The deviceinfo module must send a TransferComplete! event when the Downloading is finished.

## Release v1.2.11 - 2022-06-24(10:16:13 +0000)

### Other

- Add procps-ng as dependency

## Release v1.2.10 - 2022-06-24(08:39:13 +0000)

### Fixes

- ManufacturerOUI must uses upper-case letters

## Release v1.2.9 - 2022-06-14(07:14:44 +0000)

### Other

- Support of firmware upgrade using TR181

## Release v1.2.8 - 2022-06-10(06:36:17 +0000)

### Other

- [DeviceInfo]ProcessStatus subobject must be supported

## Release v1.2.7 - 2022-05-31(08:45:51 +0000)

### Other

- [DeviceInfo] Support DeviceInfo.Processor(NumberOfEntries)

## Release v1.2.6 - 2022-05-23(07:45:03 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v1.2.5 - 2022-05-12(11:12:54 +0000)

### Fixes

- [Debug-information] change ps utility to procps-ng-ps

## Release v1.2.4 - 2022-04-25(07:52:44 +0000)

### Fixes

- [tr181-debuginformation] upgrade ps utilisation

## Release v1.2.3 - 2022-03-29(07:52:07 +0000)

### Other

- [tr181-deviceinfo] Cannot override DeviceInfo.Description

## Release v1.2.2 - 2022-03-24(09:28:53 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.2.1 - 2022-03-22(12:53:22 +0000)

### Other

- [automated testing] not clear when the system is done initializing

## Release v1.2.0 - 2022-03-17(10:31:52 +0000)

### New

- [amx][firewall] Add a debug script for deviceInfo

## Release v1.1.1 - 2022-03-07(09:51:12 +0000)

### Fixes

- [DeviceInfo] Persistent parameters are not persistent

## Release v1.1.0 - 2022-03-04(07:37:34 +0000)

### New

- Rework environment script to handle more devices

## Release v1.0.5 - 2022-02-25(11:56:02 +0000)

### Other

- Enable core dumps by default

## Release v1.0.4 - 2022-02-22(16:36:11 +0000)

### Other

- [automated testing] not clear when the system is done initializing

## Release v1.0.3 - 2021-11-23(17:16:37 +0000)

### Other

- [ACL] The DeviceInformation plugin needs to have a default ACL configuration

## Release v1.0.2 - 2021-11-18(10:12:57 +0000)

### Changes

- lower log level

## Release v1.0.1 - 2021-06-18(14:41:49 +0000)

### Fixes

- Fix test directory name
- Remove trailing space
- Dangerous clean target for all tr181 components

### New

- add license
- add opensource url

### Changes

- change category

## Release v1.0.0 - 2021-05-04(15:20:00 +0100)

### New

- Initial release
