#!/bin/sh

prepare_environment() {
    for filename in /etc/env.d/*; do
        sh $filename
    done
}

case $1 in
    boot)
        mkdir -p /var/etc/
        prepare_environment
        ;;
    start)
        ;;
    stop)
        ;;
    shutdown)
        ;;
    restart)
        ;;
    debuginfo)
        ;;
    *)
        echo "Usage : $0 [boot]"
        ;;
esac
