#!/bin/sh

name="sysinit done"

case $1 in
    start|boot)
        echo "Sysinit done" > /dev/console
        logger "Sysinit done"
        ;;
    stop|shutdown)
        ;;
    restart)
        ;;
    *)
        echo "Usage : $0 [start|boot|stop]"
        ;;
esac
