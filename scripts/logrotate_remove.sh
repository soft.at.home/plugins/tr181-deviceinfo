#!/bin/sh
#Script installed by tr181-deviceinfo.
JSON_LIST=""

#Check for at least 3 arguments (1: rotated file, 2: DM alias, 3: Timeout to delete files)
if [ $# -lt 3 ]; then
    exit 22
fi

FILENAME_PATH=$1  # e.g: /var/log/messages
ALIAS=$2          # e.g: cpe-LogRotate-1
LIFETIME=$3       # e.g: 10

DIRECTORY=$(dirname "$FILENAME_PATH") # e.g: /var/log
FILENAME=$(basename "$FILENAME_PATH") # e.g: messages

if [ ! -d "$DIRECTORY" ]; then
    exit 2
fi

FILES=$(find "$DIRECTORY" -maxdepth 1 -type f -name "$FILENAME".* -mmin +"$LIFETIME")

if [ -z "$FILES" ]; then
    exit 2
fi

for FILE in $FILES; do
    SEPERATOR=","
    if [ -z "$JSON_LIST" ]
    then
        SEPERATOR=""
    fi

    JSON="{action=remove,alias=$ALIAS,filename=$FILE}"
    JSON_LIST="$JSON_LIST$SEPERATOR$JSON"
done
ba-cli -a "DeviceInfo.UpdateLogRotate(mod_namespace=lr-ctrl,mod_name=mod-lr-logrotate,fnc=handle-script,data=[$JSON_LIST])"
