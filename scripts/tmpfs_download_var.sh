#!/bin/sh

export PARTITION_DM_OBJECT_PATH="DeviceInfo"
PARTITION_PATH_ARG="DownloadPath"
PARTITION_SIZE_ARG="DownloadPathSize"
PARTITION_DURATION_ARG="DownloadPathDuration"

export PARTITION_DEFAULT_SIZE=10 #Default size of partition in Mb
export PARTITION_DEFAULT_PATH="/downloads/"
export PARTITION_DEFAULT_DURATION=300 #Default max time the partition will last

set +e
if [ -z "$PARAM_PARTITION_PATH" ]; then
    export PARTITION_PATH=`ba-cli "ubus-protected; pcb-protected; ${PARTITION_DM_OBJECT_PATH}.?" | grep ${PARTITION_PATH_ARG}= | head -n 1 | cut -d'=' -f2 | tr -d '"' 2> /dev/nul || true`
else
    export PARTITION_PATH=$PARAM_PARTITION_PATH
fi

if [ -z "$PARAM_PARTITION_SIZE" ]; then
    export PARTITION_SIZE=`ba-cli "ubus-protected; pcb-protected; ${PARTITION_DM_OBJECT_PATH}.?" | grep ${PARTITION_SIZE_ARG}= | head -n 1 | cut -d'=' -f2 | tr -d '"' 2> /dev/nul || true`
else
    export PARTITION_SIZE=$PARAM_PARTITION_SIZE
fi

if [ -z "$PARAM_PARTITION_DURATION" ]; then
    export PARTITION_DURATION=`ba-cli "ubus-protected; pcb-protected; ${PARTITION_DM_OBJECT_PATH}.?" | grep ${PARTITION_DURATION_ARG}= | head -n 1 | cut -d'=' -f2 | tr -d '"' 2> /dev/nul || true`
else
    export PARTITION_DURATION=$PARAM_PARTITION_DURATION
fi
set -e

#metadata files
export PARTITION_METADATA_FOLDER=".metadata"
export PARTITION_OWNER_FILE="owner"
export PARTITION_TIME_FILE="timestamp"
export PARTITION_DURATION_FILE="duration"

#Partition path file lolation
export PARTITION_SAFE_TMPFS_PARTITION="/tmpfs_temp_partition"
export PARTITION_PATH_FILE="/tmpfs_temp_partition/tmpfs_path"

#crontab /scheduler
export PARTITION_GLOBAL_CRONTAB_FILE="/etc/crontabs/root"
export PARTITION_CRONTAB_FILE="/etc/crontabs/download_tmpfs"

#other variables
export PARTITION_UNMOUNT_SCRIPT="/usr/lib/amx/scripts/tmpfs_download_unmount.sh"

#auxiliary functions

tmpfs_run_cron_service() {
    IS_CRON_RUNNING=`ps aux | grep "crond " | grep -v grep  || true`
    [ -z "$IS_CRON_RUNNING" ] && /etc/init.d/cron start
}

tmpfs_create_crontab_job() {
    [ ! -f "$PARTITION_CRONTAB_FILE" ] && return 1

    #Apply the crontab job
    if [ -f "$PARTITION_GLOBAL_CRONTAB_FILE" ]; then
        (crontab -l && cat $PARTITION_CRONTAB_FILE) | crontab -
    else
        (cat $PARTITION_CRONTAB_FILE) | crontab -
    fi

    #Run cron
    tmpfs_run_cron_service
}

tmpfs_delete_crontab_job() {
    [ ! -f "$PARTITION_CRONTAB_FILE" ] && return 1

    (crontab -l | grep -vxFf $PARTITION_CRONTAB_FILE) | crontab -

    #Run cron
    tmpfs_run_cron_service
}