#!/bin/sh

if [ -f /etc/prplconfig ]; then
    SOFTWAREVERSION=$(source /etc/prplconfig ; echo $CONFIG_SAH_AMX_TR181_DEVICEINFO_SOFTWARE_VERSION)
elif [ -f /etc/os-release ]; then
    SOFTWAREVERSION=$(source /etc/os-release ; echo $VERSION_ID)
else
    SOFTWAREVERSION=$(source /etc/build && echo $SoftwareVersion)
fi

DEVICECATEGORY=""
CID="000000"
PEN=""

#VALUES USING BASH
DESCRIPTION=$(awk '$1 ~ /DISTRIB_DESCRIPTION/' /etc/openwrt_release | cut -d "'" -f2 | cut -d "'" -f1)

echo "export DEVICECATEGORY=\"$DEVICECATEGORY\"" >> /var/etc/environment
echo "export CID=\"$CID\"" >> /var/etc/environment
echo "export PEN=\"$PEN\"" >> /var/etc/environment
echo "export DESCRIPTION=\"$DESCRIPTION\"" >> /var/etc/environment
echo "export SOFTWAREVERSION=\"$SOFTWAREVERSION\"" >> /var/etc/environment
