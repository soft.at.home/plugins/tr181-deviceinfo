#!/bin/sh

source /lib/functions/system.sh

if [[ -f "/etc/env.d/00_environment_common.sh" ]]; then
    exit 0
fi

# Hardware MAC address can be setup in variety of ways by manufacturer. Following code tries to find a good match.
# - some manufacturer set it in /proc/environment/ethaddr
# - some manufacturer sets it on kernel command line
# - Assume first physical interface assigned in br-lan has a proper MAC Address setup
if [ -f "/proc/environment/ethaddr" ]; then
    HWMACADDRESS=$(cat /proc/environment/ethaddr  | tr [a-f] [A-F])
elif grep -q ethaddr /proc/cmdline; then
    HWMACADDRESS=$(awk 'BEGIN{RS=" ";FS="="} $1 == "ethaddr" {print $2}' /proc/cmdline  | tr [a-f] [A-F])
elif [ -f "/etc/networklayout.json" ]; then
    LANIFNAME=$(jsonfilter -i "/etc/networklayout.json" -e '@.Bridges.Lan.Ports[0].Name')
    LANIFNAMEADDR=$(cat /sys/class/net/${LANIFNAME}/address | tr [a-f] [A-F])
    if [ -n "$LANIFNAMEADDR" ] ; then
        HWMACADDRESS=$LANIFNAMEADDR
    fi
fi

# Persistent MANUFACTUREROUI and SERIALNUMBER are mandatory for TR-069 compliance
# If no above source provided a MAC, generate a random one to use as serial
if [ -z $HWMACADDRESS ]; then
    dummy_mac_path="/etc/config/macaddr_generated" 
    # Check if a MAC has already been generated
    if [ -f "$dummy_mac_path" ]; then
        DUMMYMAC=$(cat "$dummy_mac_path")
    else
        DUMMYMAC=$(macaddr_random | tr '[a-z]' '[A-Z]')
        echo "$DUMMYMAC" > "$dummy_mac_path"
    fi
    HWMACADDRESS=${DUMMYMAC}
fi

MANUFACTURER=$(awk -F',' '{print $1}' /tmp/sysinfo/board_name)
PRODUCTCLASS=$(awk -F',' '{print $2}' /tmp/sysinfo/board_name)
MODELNAME=$(cat /tmp/sysinfo/model)
HARDWAREVERSION=$(awk '$1 ~ /DISTRIB_TARGET/' /etc/openwrt_release | cut -d "'" -f2 | cut -d "'" -f1)

MANUFACTUREROUI=$(echo $HWMACADDRESS | tr -d ':' | head -c 6 | tr '[a-z]' '[A-Z]')
SERIALNUMBER=SN$(echo $HWMACADDRESS | tr -d ':')

echo "export HWMACADDRESS=\"$HWMACADDRESS\"" >> /var/etc/environment
echo "export MANUFACTURER=\"$MANUFACTURER\"" >> /var/etc/environment
echo "export PRODUCTCLASS=\"$PRODUCTCLASS\"" >> /var/etc/environment
echo "export HARDWAREVERSION=\"$HARDWAREVERSION\"" >> /var/etc/environment
echo "export MANUFACTUREROUI=\"$MANUFACTUREROUI\"" >> /var/etc/environment
echo "export SERIALNUMBER=\"$SERIALNUMBER\"" >> /var/etc/environment
echo "export MODELNAME=\"$MODELNAME\"" >> /var/etc/environment
