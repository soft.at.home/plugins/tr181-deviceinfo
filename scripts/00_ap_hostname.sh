#!/bin/sh

if [ -f "/proc/environment/ethaddr" ]; then
    MY_MACADDRESS=$(cat /proc/environment/ethaddr)
elif uci -q get network.lan.macaddr; then
    MY_MACADDRESS=$(uci get network.lan.macaddr)
elif [ -f "/sys/class/net/br-lan/address" ]; then
    MY_MACADDRESS=$(cat /sys/class/net/br-lan/address)
elif grep -q ethaddr /proc/cmdline; then
    MY_MACADDRESS=$(awk 'BEGIN{RS=" ";FS="="} $1 == "ethaddr" {print $2}' /proc/cmdline)
elif [ -f "/etc/networklayout.json" ]; then
    LANIFNAME=$(jsonfilter -i "/etc/networklayout.json" -e '@.Bridges.Lan.Ports[0].Name')
    LANIFNAMEADDR=$(cat /sys/class/net/${LANIFNAME}/address | tr [a-f] [A-F])
    if [ -n "$LANIFNAMEADDR" ] ; then
        MY_MACADDRESS=$LANIFNAMEADDR
    fi
fi

mac_address=$(echo $MY_MACADDRESS | grep -o -E '([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}' | tail -n 1 | awk -F ':' '{print $4$5$6}')

# create the hostname
if [ -n "$mac_address" ] ; then
    MY_HOSTNAME="prplAP-$mac_address"
else
    MY_HOSTNAME="prplAP"
fi

echo "export MY_HOSTNAME=\"$MY_HOSTNAME\"" >> /var/etc/environment
