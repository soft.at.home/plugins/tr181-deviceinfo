#!/bin/sh

####################################################################
#How to use: tmpfs_download_unmount.sh PROC_OWNER
#
#PROC_OWNER - The name of the plugin or process that is executing the script, the partition
#             manipulation will be locked to this specific owner.
#
####################################################################

lockfile="/tmp/tmpfs_download_script.lock"
while [ -e "$lockfile" ]; do NOP_VAR=""; done
touch "$lockfile"
trap 'rm -f "$lockfile"' EXIT

PROC_OWNER="$1" #Name of the plugin or process executing the script
export PARAM_PARTITION_PATH="$2"
export PARAM_PARTITION_SIZE="$3"
export PARAM_PARTITION_DURATION="$4"

. /usr/lib/amx/scripts/tmpfs_download_var.sh

[ -z "$PROC_OWNER" ] && exit 1

#Validate variables from /usr/lib/amx/scripts/tmpfs_download_var.sh
[ -z "$PARTITION_CRONTAB_FILE" ] && exit 1
[ -z "$PARTITION_GLOBAL_CRONTAB_FILE" ] && exit 1

if [ -z "$PARTITION_SIZE" ]; then
    PARTITION_SIZE=$PARTITION_DEFAULT_SIZE
    if [ -z "$PARTITION_SIZE" ]; then
        PARTITION_SIZE=0
    fi
fi
! expr "$PARTITION_SIZE" : '[0-9]\+$' > /dev/null && exit 1 #Check if PARTITION_SIZE is an integer

if [ -z "$PARTITION_PATH" ]; then
    PARTITION_PATH=`head -n 1 $PARTITION_PATH_FILE | cut -d' ' -f1 2> /dev/null || true`
    if [ -z "$PARTITION_PATH" ]; then
        PARTITION_PATH=$PARTITION_DEFAULT_PATH
    fi
fi
[ -z "$PARTITION_PATH" ] && exit 1

#Avoid doing stuff on unwanted directories
case "$PARTITION_PATH" in
    "/" | \
    *"/bin"* | \
    *"/bootfs"* | \
    *"/cfg"* | \
    *"/data"* | \
    *"/dev"* | \
    *"/etc"* | \
    *"/home"* | \
    *"/include"* | \
    *"/lcm"* | \
    *"/lib"* | \
    *"/lib64"* | \
    *"/mnt"* | \
    *"/overlay"* | \
    *"/proc"* | \
    *"/rom"* | \
    *"/root"* | \
    *"/sbin"* | \
    *"/sys"* | \
    *"/tmp"* | \
    *"/usr"* | \
    *"/webui"* | \
    *"/www"*)
        exit
        ;;
esac

#The PARTITION_OWNER_FILE is a file that contains the name of the process or plugin that mounted the partition. 
CURRENT_OWNER=`head -n 1 $PARTITION_PATH/$PARTITION_METADATA_FOLDER/$PARTITION_OWNER_FILE | cut -d' ' -f1 2> /dev/null || true`
if [[ "$PROC_OWNER" != "$CURRENT_OWNER" ]] && [[ -n "$CURRENT_OWNER" ]] && [[ "$PROC_OWNER" != "root" ]]; then
    exit 1
fi

echo "Release tmpfs partition at ${PARTITION_PATH}."

if [ -n "$PARTITION_METADATA_FOLDER" ]; then
    mount -o remount,rw ${PARTITION_PATH}/$PARTITION_METADATA_FOLDER || true
    rm -rf ${PARTITION_PATH}/$PARTITION_METADATA_FOLDER/* || true
    umount ${PARTITION_PATH}/$PARTITION_METADATA_FOLDER || true
    rm -rf ${PARTITION_PATH}/$PARTITION_METADATA_FOLDER || true
fi

if [ -n "$PARTITION_SAFE_TMPFS_PARTITION" ]; then
    mount -o remount,rw $PARTITION_SAFE_TMPFS_PARTITION || true
    rm -rf $PARTITION_SAFE_TMPFS_PARTITION/* || true
    umount $PARTITION_SAFE_TMPFS_PARTITION || true
    rm -rf $PARTITION_SAFE_TMPFS_PARTITION || true
fi

rm -rf ${PARTITION_PATH}/* || true
#If dynamically created tmpfs is used,then use umount instead of remount
#Not used at the moment due to permission reasons. tmpfs is mounted once during boot.
#umount ${PARTITION_PATH} || true
mount -o remount,size=${PARTITION_SIZE}M $PARTITION_PATH || true

#Apply the crontab job
tmpfs_delete_crontab_job
