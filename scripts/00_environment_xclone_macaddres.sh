#!/bin/sh

# Check if /etc/config/clone_macaddress exists
if [ -f /etc/config/clone_macaddress ]; then
    # If the file exists, read BASEMACADDRESS from it
    CLONEMACADDRESS=$(grep '^MACADDRESS=' /etc/config/clone_macaddress | cut -d'=' -f2)
    
    # Check if CLONEMACADDRESS has a valid MAC address format
    if echo "$CLONEMACADDRESS" | grep -Eq '^([0-9A-Fa-f]{2}:){5}[0-9A-Fa-f]{2}$'; then
        echo "Valid MAC address: $CLONEMACADDRESS"
        echo "export CLONEMACADDRESS=\"$CLONEMACADDRESS\"" >> /var/etc/environment
    fi
fi