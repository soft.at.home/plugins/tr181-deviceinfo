#!/bin/sh

# Creation of the hash
# know what rootfs mode we are using

PRPL_VERSIONS_FILE="/tmp/sw-versions-prpl"

. /lib/prpl-layout-partlabels #Environment variables with the partitioning standard installed by the board-configurator package

mode=`fw_printenv -n mode 2> /dev/null || true`

fit_tools_version_cmd() {
    local dev_file="$1"
    fit_tools info "$dev_file" -q -p s,/version -u /security/sbl/rootfs.pubkey.raw 2> /dev/null
}

get_version_from_tmp() {
    local partlabel="$1"
    local file_content=""
    
    if [ -n "$partlabel" ]; then
        file_content=`cat $PRPL_VERSIONS_FILE | grep "$partlabel " 2> /dev/null || true`
        version=`echo "$file_content" | cut -d' ' -f2 2> /dev/null || true`

        if [ -z "$version" ]; then
            [ -f "/usr/bin/scan_prpl_partitions_versions" ] && /usr/bin/scan_prpl_partitions_versions &> /dev/null || true
            file_content=`cat $PRPL_VERSIONS_FILE | grep "$partlabel " 2> /dev/null || true`
            version=`echo "$file_content" | cut -d' ' -f2 2> /dev/null || true`
        fi

        echo "$version"
    fi
}

print_fit_version() {
    local dev_path="$1"
    local partlabel="$2"
    local fit_version=""
    for dev_file in "$dev_path"*; do
        if [ ! -f "$dev_file" ]; then
            if [ ! -L "$dev_file" ]; then
                continue
            fi
        fi
        fit_version=`fit_tools_version_cmd "$dev_file" 2> /dev/null || true`
        if [ -n "$fit_version" ]; then
            break
        fi
    done

    if [ -z "$fit_version" ]; then
        fit_version=`get_version_from_tmp "$partlabel" 2> /dev/null || true`
    fi

    echo "$fit_version"
}

if [ "$mode" == "nominal" ]; then
    ROOTFS_DEV_PATH="$PRPL_LAYOUT_PART_PATH/$PRPL_LAYOUT_ROOTFS_ACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
else
    ROOTFS_DEV_PATH="$PRPL_LAYOUT_PART_PATH/$PRPL_LAYOUT_ROOTFS_INACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
fi

if [ -z "$mode" ]; then
    ROOTFS_DEV_PATH="$PRPL_LAYOUT_PART_PATH/$PRPL_LAYOUT_ROOTFS_ACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
fi

[ -z "$firm_version" ] && firm_version=`print_fit_version "$ROOTFS_DEV_PATH" "$PRPL_LAYOUT_ROOTFS_ACTIVE" 2> /dev/null || true`
[ -z "$firm_version" ] && firm_version=`grep "BUILD_CONFIG_VERSION" /version.txt | awk -F '=' '{print $2}' 2> /dev/null || true`
[ -z "$firm_version" ] && firm_version=`grep "VERSION_ID" /etc/os-release | awk -F '"' '{print $2}' 2> /dev/null || true`

if [ "$firm_version" == "master" ]; then
    firm_version=`grep "BUILD_START" /version.txt | awk -F '=' '{print $2}' 2> /dev/null || true`
fi

os_release=`grep "BUILD_ID" /etc/os-release | awk -F '"' '{print $2}' 2> /dev/null || true`

combined_string="$firm_version$os_release"

hash=`echo -n "$combined_string" | sha256sum | cut -d " " -f 1`

echo $hash
