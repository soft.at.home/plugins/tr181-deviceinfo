#!/bin/sh

case $1 in
    start|boot)
        hash=$(software_version_hash.sh)
        echo "$hash" > /cfg/dbversion
        ;;
    stop|shutdown)
        ;;
    restart)
        ;;
    *)
        echo "Usage : $0 [start|boot|stop]"
        ;;
esac
