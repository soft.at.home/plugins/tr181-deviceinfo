/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include "deviceinfo_vendor_config_file.h"
#include "filetransfer/filetransfer.h"
#include "deviceinfo.h"
#include "test_vendor_config_file.h"
#include "common_mock.h"

static int create_file(const char* filename, const char* data) {
    int retval = -1;
    FILE* fd = NULL;
    when_str_empty(filename, exit);
    when_str_empty(data, exit);
    fd = fopen(filename, "w");
    when_null(fd, exit);
    fprintf(fd, "%s", data);
    fclose(fd);

    retval = 0;

exit:
    return retval;
}

static inline amxd_object_t* get_vendor_config_file_object(void) {
    return amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorConfigFile.");
}

static amxd_object_t* vendor_config_file_add_instance(const char* alias, const char* name,
                                                      const bool use_for_backup_restore) {
    int retval = -1;
    amxd_object_t* vendor_config_file_object = NULL;
    amxd_object_t* vendor_config_file_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, parameters, "Name", name);
    amxc_var_add_key(bool, parameters, "UseForBackupRestore", use_for_backup_restore);

    vendor_config_file_object = get_vendor_config_file_object();
    assert_non_null(vendor_config_file_object);

    retval = amxd_object_add_instance(&vendor_config_file_instance, vendor_config_file_object, alias, 0, parameters);
    assert_int_equal(retval, 0);

    amxc_var_delete(&parameters);
    return vendor_config_file_instance;
}

int test_vendor_config_file_setup(void** state) {
    return mock_setup(state, NULL);
}

int test_vendor_config_file_teardown(void** state) {
    return mock_teardown(state);
}

void test_backup(UNUSED void** state) {
    amxd_object_t* intf = vendor_config_file_add_instance("cpe-test-1", "testFile", 1);
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "http://test");
    amxc_var_add_key(cstring_t, &args, "Username", "");
    amxc_var_add_key(cstring_t, &args, "Password", "");

    assert_non_null(intf);
    assert_int_equal(amxd_object_invoke_function(intf, "Backup", &args, &retval), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_restore(UNUSED void** state) {
    amxd_object_t* intf = vendor_config_file_add_instance("cpe-test-2", "testFile", 1);
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "http://test");
    amxc_var_add_key(cstring_t, &args, "Username", "");
    amxc_var_add_key(cstring_t, &args, "Password", "");
    amxc_var_add_key(cstring_t, &args, "TargetFileName", "test");

    assert_non_null(intf);
    assert_int_equal(amxd_object_invoke_function(intf, "Restore", &args, &retval), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_backup_no_schema(UNUSED void** state) {
    amxd_object_t* intf = vendor_config_file_add_instance("cpe-test-3", "testFile", 1);
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "test");
    amxc_var_add_key(cstring_t, &args, "Username", "");
    amxc_var_add_key(cstring_t, &args, "Password", "");

    assert_non_null(intf);
    assert_int_equal(amxd_object_invoke_function(intf, "Backup", &args, &retval), amxd_status_unknown_error);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_restore_no_schema(UNUSED void** state) {
    amxd_object_t* intf = vendor_config_file_add_instance("cpe-test-4", "/tmp/dummy.txt", 1);
    amxc_var_t retval;
    amxc_var_t args;
    const char* dummy_file = "/tmp/dummy.txt";

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "test");
    amxc_var_add_key(cstring_t, &args, "Username", "");
    amxc_var_add_key(cstring_t, &args, "Password", "");
    amxc_var_add_key(cstring_t, &args, "TargetFileName", "test");

    assert_int_equal(create_file(dummy_file, "dummy data"), 0);
    assert_non_null(intf);
    assert_int_equal(amxd_object_invoke_function(intf, "Restore", &args, &retval), amxd_status_unknown_error);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
    remove(dummy_file);
}

void test_restore_no_target_file(UNUSED void** state) {
    amxd_object_t* intf = vendor_config_file_add_instance("cpe-test-5", "/tmp/dummy.txt", 1);
    amxc_var_t retval;
    amxc_var_t args;
    const char* dummy_file = "/tmp/dummy.txt";

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "http://test");
    amxc_var_add_key(cstring_t, &args, "Username", "");
    amxc_var_add_key(cstring_t, &args, "Password", "");

    assert_int_equal(create_file(dummy_file, "dummy data"), 0);
    assert_non_null(intf);
    assert_int_equal(amxd_object_invoke_function(intf, "Restore", &args, &retval), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
    remove(dummy_file);
}