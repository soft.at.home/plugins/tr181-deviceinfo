MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../mocks)
MOCK_SRCDIR = $(realpath ../mocks/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

WRAP_FUNC=-Wl,--wrap=
MOCK_WRAP = amxb_be_who_has \
            amxm_so_open \
            amxm_close_all \
            amxm_execute_function

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral -Wno-unused-function\
		  $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxo -lamxb -lamxm -lamxut \
		   -lsahtrace -ldl -lpthread -lfiletransfer
