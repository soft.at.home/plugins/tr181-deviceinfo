/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "deviceinfo_dm_mngr.h"
#include "deviceinfo.h"
#include "deviceinfo_logrotate.h"
#include "test_logrotate.h"
#include "common_mock.h"

static void cleanup_entry(const char* name) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);

    amxd_trans_select_pathf(&trans, "DeviceInfo.LogRotate");
    amxd_trans_del_inst(&trans, 0, name);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxut_bus_handle_events();

    amxd_trans_clean(&trans);
}

static void assert_nr_logrotates(uint32_t expected_nr) {
    amxd_object_t* logrotate_templ_obj = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.");
    assert_non_null(logrotate_templ_obj);

    assert_int_equal(amxc_llist_size(&logrotate_templ_obj->instances), expected_nr);
}

int test_logrotate_setup(void** state) {
    return mock_setup(state, NULL);
}

int test_logrotate_teardown(void** state) {
    return mock_teardown(state);
}

void test_change_interval(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &data, "LogRotateInterval", 5);
    assert_int_equal(amxb_set(amxut_bus_ctx(), "DeviceInfo.", &data, &ret, 5), 0);
    amxc_var_clean(&data);
    amxut_bus_handle_events();
    assert_nr_logrotates(0);

    amxc_var_clean(&ret);
}

void test_add_entries(UNUSED void** state) {
    amxd_trans_t trans;
    assert_nr_logrotates(0);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);
    amxd_trans_select_pathf(&trans, "DeviceInfo.LogRotate");
    amxd_trans_add_inst(&trans, 0, "test_entry");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "Name", "file:///var/log/messages");
    amxd_trans_set_value(uint32_t, &trans, "NumberOfFiles", 3);
    amxd_trans_set_value(uint32_t, &trans, "MaxFileSize", 5000);
    amxd_trans_set_value(uint32_t, &trans, "RollOver", 5);
    amxd_trans_set_value(uint32_t, &trans, "Retention", 7);
    amxd_trans_set_value(cstring_t, &trans, "Compression", "GZIP");
    amxd_trans_set_value(bool, &trans, "DelayCompression", false);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
    assert_nr_logrotates(1);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);
    amxd_trans_select_pathf(&trans, "DeviceInfo.LogRotate");
    amxd_trans_add_inst(&trans, 0, "test_entry2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Name", "file:/var/log/important");
    amxd_trans_set_value(uint32_t, &trans, "NumberOfFiles", 5);
    amxd_trans_set_value(uint32_t, &trans, "MaxFileSize", 200);
    amxd_trans_set_value(uint32_t, &trans, "RollOver", 10);
    amxd_trans_set_value(uint32_t, &trans, "Retention", 60);
    amxd_trans_set_value(cstring_t, &trans, "Compression", "None");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);
}

void test_change_entry(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &data, "NumberOfFiles", 30);
    amxc_var_add_key(uint32_t, &data, "MaxFileSize", 5);
    amxc_var_add_key(uint32_t, &data, "RollOver", 2);
    amxc_var_add_key(uint32_t, &data, "Retention", 9);
    amxc_var_add_key(cstring_t, &data, "Compression", "None");
    amxc_var_add_key(bool, &data, "DelayCompression", true);
    assert_int_equal(amxb_set(amxut_bus_ctx(), "DeviceInfo.LogRotate.test_entry.", &data, &ret, 5), 0);
    amxc_var_clean(&data);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Name", "file:/var/log/minor");
    assert_int_equal(amxb_set(amxut_bus_ctx(), "DeviceInfo.LogRotate.test_entry2.", &data, &ret, 5), 0);
    amxc_var_clean(&data);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&ret);
}

void test_enable_entry(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &data, "Enable", true);
    assert_int_equal(amxb_set(amxut_bus_ctx(), "DeviceInfo.LogRotate.test_entry.", &data, &ret, 5), 0);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&ret);
    amxc_var_clean(&data);
}

void test_change_enabled_entry(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, &data, "NumberOfFiles", 10);
    amxc_var_add_key(uint32_t, &data, "Retention", 50);
    amxc_var_add_key(uint32_t, &data, "RollOver", 3);
    amxc_var_add_key(cstring_t, &data, "Compression", "GZIP");
    amxc_var_add_key(bool, &data, "DelayCompression", false);
    assert_int_equal(amxb_set(amxut_bus_ctx(), "DeviceInfo.LogRotate.test_entry.", &data, &ret, 5), 0);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&ret);
    amxc_var_clean(&data);
}

void test_add_logfiles(UNUSED void** state) {
    amxc_var_t fncdata;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxc_var_init(&fncdata);
    amxc_var_set_type(&fncdata, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &fncdata, "mod_namespace", "lr-ctrl");
    amxc_var_add_key(cstring_t, &fncdata, "mod_name", "mod-lr-logrotate");
    amxc_var_add_key(cstring_t, &fncdata, "fnc", "handle-script");

    // Fails since it's missing data
    assert_int_equal(amxb_call(amxut_bus_ctx(), "DeviceInfo.", "UpdateLogRotate", &fncdata, &ret, 5), amxd_status_invalid_function_argument);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&fncdata);
    amxc_var_clean(&ret);
}

void test_add_logfiles2(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_t* logfile1 = NULL;
    amxc_var_t* logfile2 = NULL;
    amxc_ts_t ts;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);
    amxc_ts_parse(&ts, "2024-02-22T17:55:31Z", 20);
    logfile1 = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, logfile1, "Action", "add");
    amxc_var_add_key(cstring_t, logfile1, "Name", "file:test_entry.1");
    amxc_var_add_key(uint32_t, logfile1, "Size", 523);
    amxc_var_add_key(amxc_ts_t, logfile1, "LastChange", &ts);
    assert_int_equal(dm_update_logrotate_logfile(NULL, &data, NULL), -1); // Missing ParentAlias
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_add_key(cstring_t, logfile1, "ParentAlias", "test_entry");
    assert_int_equal(dm_update_logrotate_logfile(NULL, &data, NULL), 0);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);
    amxc_ts_parse(&ts, "2024-02-22T17:58:31Z", 20);
    logfile1 = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, logfile1, "Action", "add");
    amxc_var_add_key(cstring_t, logfile1, "ParentAlias", "test_entry");
    amxc_var_add_key(cstring_t, logfile1, "Name", "file:test_entry.1");
    amxc_var_add_key(uint32_t, logfile1, "Size", 623);
    amxc_var_add_key(amxc_ts_t, logfile1, "LastChange", &ts);
    amxc_ts_parse(&ts, "2024-02-22T18:03:31Z", 20);
    logfile2 = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, logfile2, "Action", "add");
    amxc_var_add_key(cstring_t, logfile2, "ParentAlias", "test_entry");
    amxc_var_add_key(cstring_t, logfile2, "Name", "file:test_entry.2");
    amxc_var_add_key(uint32_t, logfile2, "Size", 6230);
    amxc_var_add_key(amxc_ts_t, logfile2, "LastChange", &ts);
    assert_int_equal(dm_update_logrotate_logfile(NULL, &data, NULL), 0);
    amxut_bus_handle_events();

    assert_nr_logrotates(2);

    amxc_var_clean(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);
    logfile1 = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, logfile1, "Action", "remove");
    amxc_var_add_key(cstring_t, logfile1, "ParentAlias", "test_entry");
    amxc_var_add_key(cstring_t, logfile1, "Name", "file:test_entry.1");
    assert_int_equal(dm_update_logrotate_logfile(NULL, &data, NULL), 0);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_disable_entry(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &data, "Enable", false);
    assert_int_equal(amxb_set(amxut_bus_ctx(), "DeviceInfo.LogRotate.test_entry.", &data, &ret, 5), 0);
    amxut_bus_handle_events();
    assert_nr_logrotates(2);

    amxc_var_clean(&ret);
    amxc_var_clean(&data);
}

void test_remove_entry(UNUSED void** state) {
    cleanup_entry("test_entry");
    assert_nr_logrotates(1);
}

void test_dm_update_logrotate_add(UNUSED void** state) {
    amxc_var_t list;
    amxc_var_t* logfile1 = NULL;
    amxc_var_t* logfile2 = NULL;
    amxc_var_t* logfile3 = NULL;
    const char* filename = NULL;

    assert_nr_logrotates(0);

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    logfile1 = amxc_var_add(amxc_htable_t, &list, NULL);
    logfile2 = amxc_var_add(amxc_htable_t, &list, NULL);
    logfile3 = amxc_var_add(amxc_htable_t, &list, NULL);

    amxc_var_add_key(cstring_t, logfile1, "Action", "add");
    amxc_var_add_key(cstring_t, logfile1, "SyslogActionAlias", "syslog-messages");
    amxc_var_add_key(cstring_t, logfile1, "SyslogActionLogFilePath", "file:///var/log/messages");

    amxc_var_add_key(cstring_t, logfile2, "Action", "add");
    amxc_var_add_key(cstring_t, logfile2, "SyslogActionAlias", "syslog-firewall");
    amxc_var_add_key(cstring_t, logfile2, "SyslogActionLogFilePath", "file:///var/log/firewall");

    amxc_var_add_key(cstring_t, logfile3, "Action", "add");
    amxc_var_add_key(cstring_t, logfile3, "SyslogActionLogFilePath", "file:///var/log/error");

    assert_int_equal(dm_update_logrotate(NULL, &list, NULL), -1); // Missing alias for logfile3
    amxut_bus_handle_events();
    assert_nr_logrotates(0);

    amxc_var_add_key(cstring_t, logfile3, "SyslogActionAlias", "syslog-error");
    assert_int_equal(dm_update_logrotate(NULL, &list, NULL), 0);

    assert_nr_logrotates(3);
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.1."), "Name");
    assert_string_equal(filename, "file:///var/log/messages");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.2."), "Name");
    assert_string_equal(filename, "file:///var/log/firewall");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.3."), "Name");
    assert_string_equal(filename, "file:///var/log/error");

    amxc_var_clean(&list);
}

void test_dm_update_logrotate_change(UNUSED void** state) {
    amxc_var_t list;
    amxc_var_t* logfile1 = NULL;
    amxc_var_t* logfile2 = NULL;
    const char* filename = NULL;

    assert_nr_logrotates(3);

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    logfile1 = amxc_var_add(amxc_htable_t, &list, NULL);
    logfile2 = amxc_var_add(amxc_htable_t, &list, NULL);

    amxc_var_add_key(cstring_t, logfile1, "Action", "update");
    amxc_var_add_key(cstring_t, logfile1, "SyslogActionLogFilePath", "file:///data/log/messages");

    amxc_var_add_key(cstring_t, logfile2, "Action", "update");
    amxc_var_add_key(cstring_t, logfile2, "SyslogActionAlias", "syslog-firewall");
    amxc_var_add_key(cstring_t, logfile2, "SyslogActionLogFilePath", "file:///data/log/firewall");

    assert_int_equal(dm_update_logrotate(NULL, &list, NULL), -1);
    amxut_bus_handle_events();
    assert_nr_logrotates(3);

    amxc_var_add_key(cstring_t, logfile1, "SyslogActionAlias", "syslog-messages");
    assert_int_equal(dm_update_logrotate(NULL, &list, NULL), 0);
    amxut_bus_handle_events();

    assert_nr_logrotates(3);
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.1."), "Name");
    assert_string_equal(filename, "file:///data/log/messages");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.2."), "Name");
    assert_string_equal(filename, "file:///data/log/firewall");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.3."), "Name");
    assert_string_equal(filename, "file:///var/log/error");

    amxc_var_clean(&list);
}

void test_dm_update_logrotate_remove(UNUSED void** state) {
    amxc_var_t list;
    amxc_var_t* logfile = NULL;
    const char* filename = NULL;

    assert_nr_logrotates(3);

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    logfile = amxc_var_add(amxc_htable_t, &list, NULL);

    amxc_var_add_key(cstring_t, logfile, "Action", "remove");
    amxc_var_add_key(cstring_t, logfile, "SyslogActionLogFilePath", "file:///data/log/messages");

    assert_int_equal(dm_update_logrotate(NULL, &list, NULL), -1);
    amxut_bus_handle_events();
    assert_nr_logrotates(3);

    amxc_var_add_key(cstring_t, logfile, "SyslogActionAlias", "syslog-messages");
    assert_int_equal(dm_update_logrotate(NULL, &list, NULL), 0);
    amxut_bus_handle_events();

    assert_nr_logrotates(2);
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.2."), "Name");
    assert_string_equal(filename, "file:///data/log/firewall");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.LogRotate.3."), "Name");
    assert_string_equal(filename, "file:///var/log/error");

    amxc_var_clean(&list);
}
