/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "deviceinfo_vendor_log_file.h"
#include "filetransfer/filetransfer.h"
#include "deviceinfo.h"
#include "test_vendor_log_file.h"
#include "common_mock.h"

static inline amxd_object_t* get_vendor_log_file_object(void) {
    return amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.");
}

static amxd_object_t* vendor_log_file_add_instance(const char* alias, const char* name,
                                                   const uint32_t maximum_size, const bool persistent) {
    int retval = -1;
    amxd_object_t* vendor_log_file_object = NULL;
    amxd_object_t* vendor_log_file_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, parameters, "Name", name);
    amxc_var_add_key(uint32_t, parameters, "MaximumSize", maximum_size);
    amxc_var_add_key(bool, parameters, "Persistent", persistent);


    vendor_log_file_object = get_vendor_log_file_object();
    assert_non_null(vendor_log_file_object);

    retval = amxd_object_add_instance(&vendor_log_file_instance, vendor_log_file_object, alias, 0, parameters);
    assert_int_equal(retval, 0);

    amxc_var_delete(&parameters);

    return vendor_log_file_instance;
}

int test_vendor_log_file_setup(void** state) {
    return mock_setup(state, NULL);
}

int test_vendor_log_file_teardown(void** state) {
    return mock_teardown(state);
}

static void assert_nr_vendorlogfiles(uint32_t expected_nr) {
    amxd_object_t* vendorlogfile_templ_obj = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.");
    assert_non_null(vendorlogfile_templ_obj);

    assert_int_equal(amxc_llist_size(&vendorlogfile_templ_obj->instances), expected_nr);
}

void test_dm_update_vendorlogfile_add(UNUSED void** state) {
    amxc_var_t list;
    amxc_var_t* logfile1 = NULL;
    amxc_var_t* logfile2 = NULL;
    amxc_var_t* logfile3 = NULL;
    const char* filename = NULL;

    assert_nr_vendorlogfiles(0);

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    logfile1 = amxc_var_add(amxc_htable_t, &list, NULL);
    logfile2 = amxc_var_add(amxc_htable_t, &list, NULL);
    logfile3 = amxc_var_add(amxc_htable_t, &list, NULL);

    amxc_var_add_key(cstring_t, logfile1, "Action", "add");
    amxc_var_add_key(uint32_t, logfile1, "MaximumSize", 0);
    amxc_var_add_key(bool, logfile1, "Persistent", false);
    amxc_var_add_key(cstring_t, logfile1, "SyslogActionLogFilePath", "file:///var/log/messages");
    amxc_var_add_key(cstring_t, logfile1, "SyslogLogFileRef", "Syslog.Action.1.LogFile.");
    amxc_var_add_key(cstring_t, logfile1, "SyslogVendorLogFileRef", "");

    amxc_var_add_key(cstring_t, logfile2, "Action", "add");
    amxc_var_add_key(uint32_t, logfile2, "MaximumSize", 0);
    amxc_var_add_key(bool, logfile2, "Persistent", false);
    amxc_var_add_key(cstring_t, logfile2, "SyslogLogFileRef", "Syslog.Action.2.LogFile.");
    amxc_var_add_key(cstring_t, logfile2, "SyslogVendorLogFileRef", "");

    amxc_var_add_key(cstring_t, logfile3, "Action", "add");
    amxc_var_add_key(uint32_t, logfile3, "MaximumSize", 0);
    amxc_var_add_key(bool, logfile3, "Persistent", false);
    amxc_var_add_key(cstring_t, logfile3, "SyslogActionLogFilePath", "file:///var/log/error");
    amxc_var_add_key(cstring_t, logfile3, "SyslogLogFileRef", "Syslog.Action.3.LogFile.");
    amxc_var_add_key(cstring_t, logfile3, "SyslogVendorLogFileRef", "");

    assert_int_equal(dm_update_vendorlogfile(NULL, &list, NULL), -1); // Missing SyslogActionLogFilePath for logfile 2
    amxut_bus_handle_events();
    assert_nr_vendorlogfiles(0);

    amxc_var_add_key(cstring_t, logfile2, "SyslogActionLogFilePath", "file:///var/log/firewall");
    assert_int_equal(dm_update_vendorlogfile(NULL, &list, NULL), 0);
    amxut_bus_handle_events();

    assert_nr_vendorlogfiles(3);
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.1."), "Name");
    assert_string_equal(filename, "file:///var/log/messages");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.2."), "Name");
    assert_string_equal(filename, "file:///var/log/firewall");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.3."), "Name");
    assert_string_equal(filename, "file:///var/log/error");

    amxc_var_clean(&list);
}

void test_dm_update_vendorlogfile_change(UNUSED void** state) {
    amxc_var_t list;
    amxc_var_t* logfile1 = NULL;
    amxc_var_t* logfile2 = NULL;
    const char* filename = NULL;

    assert_nr_vendorlogfiles(3);

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    logfile1 = amxc_var_add(amxc_htable_t, &list, NULL);
    logfile2 = amxc_var_add(amxc_htable_t, &list, NULL);

    amxc_var_add_key(cstring_t, logfile1, "Action", "update");
    amxc_var_add_key(cstring_t, logfile1, "SyslogVendorLogFileRef", "DeviceInfo.VendorLogFile.1.");

    amxc_var_add_key(cstring_t, logfile2, "Action", "update");
    amxc_var_add_key(cstring_t, logfile2, "SyslogActionLogFilePath", "file:///data/log/firewall");

    assert_int_equal(dm_update_vendorlogfile(NULL, &list, NULL), -1); // Missing SyslogVendorLogFileRef for logfile2
    amxut_bus_handle_events();
    assert_nr_vendorlogfiles(3);

    amxc_var_add_key(cstring_t, logfile2, "SyslogVendorLogFileRef", "DeviceInfo.VendorLogFile.2.");
    assert_int_equal(dm_update_vendorlogfile(NULL, &list, NULL), -1); // Missing SyslogActionLogFilePath for logfile1
    amxut_bus_handle_events();
    assert_nr_vendorlogfiles(3);

    amxc_var_add_key(cstring_t, logfile1, "SyslogActionLogFilePath", "file:///data/log/messages");
    assert_int_equal(dm_update_vendorlogfile(NULL, &list, NULL), 0);
    amxut_bus_handle_events();
    assert_nr_vendorlogfiles(3);

    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.1."), "Name");
    assert_string_equal(filename, "file:///data/log/messages");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.2."), "Name");
    assert_string_equal(filename, "file:///data/log/firewall");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.3."), "Name");
    assert_string_equal(filename, "file:///var/log/error");

    amxc_var_clean(&list);
}

void test_dm_update_vendorlogfile_remove(UNUSED void** state) {
    amxc_var_t list;
    amxc_var_t* logfile = NULL;
    const char* filename = NULL;

    assert_nr_vendorlogfiles(3);

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    logfile = amxc_var_add(amxc_htable_t, &list, NULL);

    amxc_var_add_key(cstring_t, logfile, "Action", "remove");
    amxc_var_add_key(cstring_t, logfile, "SyslogActionLogFilePath", "file:///data/log/messages");

    assert_int_equal(dm_update_vendorlogfile(NULL, &list, NULL), 0);
    amxut_bus_handle_events();

    assert_nr_vendorlogfiles(2);
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.2."), "Name");
    assert_string_equal(filename, "file:///data/log/firewall");
    filename = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.VendorLogFile.3."), "Name");
    assert_string_equal(filename, "file:///var/log/error");

    amxc_var_clean(&list);
}

void test_upload(UNUSED void** state) {
    amxd_object_t* intf = vendor_log_file_add_instance("cpe-test-1", "testFile", 0, 0);
    amxc_var_t retval;
    amxc_var_t args;
    ftx_request_t* request = NULL;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "http://test");
    amxc_var_add_key(cstring_t, &args, "Username", "");
    amxc_var_add_key(cstring_t, &args, "Password", "");

    assert_non_null(intf);
    assert_int_equal(amxd_object_invoke_function(intf, "Upload", &args, &retval), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
    ftx_request_delete(&request);
}


